function goToList() {
	window.location = 'termList';

}

function selectDiscipline() {
	var aDisciplinesElement = document
			.getElementById("availableDisciplinesList");
	var sDisciplinesElement = document
			.getElementById("selectedDisciplinesList");

	var allOpts = aDisciplinesElement.childNodes;

	for (var i = 0; i < allOpts.length; i++) {
		if (allOpts[i].selected) {
			sDisciplinesElement.appendChild(allOpts[i].cloneNode(true));
			aDisciplinesElement.removeChild(allOpts[i]);
		}
	}
	updateSelectedValue();
}

function unselectDiscipline() {
	var aDisciplinesElement = document
			.getElementById("availableDisciplinesList");
	var sDisciplinesElement = document
			.getElementById("selectedDisciplinesList");

	var allOpts = sDisciplinesElement.childNodes;

	for (var i = 0; i < allOpts.length; i++) {
		if (allOpts[i].selected) {
			aDisciplinesElement.appendChild(allOpts[i].cloneNode(true));
			sDisciplinesElement.removeChild(allOpts[i]);
		}
	}
	updateSelectedValue();
}

function updateSelectedValue() {
	var sDisciplinesElement = document.getElementById("selectedDisciplines");
	var selectedDisciplinesList = document
			.getElementById("selectedDisciplinesList");

	var allOpts = selectedDisciplinesList.childNodes;

	var resultList = "";
	for (var i = 0; i < allOpts.length; i++) {
		if (allOpts[i].tagName && allOpts[i].tagName.toLowerCase() == "option") {
			if (i < allOpts.length - 1) {
				resultList += allOpts[i].value + ", ";
			} else {
				resultList += allOpts[i].value;
			}
		}
	}

	sDisciplinesElement.value = resultList;
}
