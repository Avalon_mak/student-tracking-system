function goToTermDetails(id) {
	window.location = 'termDetails?id='
			+ id;
}

function goToTermEdit() {
	var list = collectCheckboxes();
	var checked = collectCheckedCheckboxes(list);

	if (checked.length > 1) {
		alert("Take only ONE discipline");
	} else if (checked.length == 0) {
		alert("Select a discipline");
	} else {
		window.location = 'termEdit?id='
				+ checked[0].attributes.id.value;
	}
}

function collectCheckedCheckboxes(checkboxes) {
	var checked = [];
	for (var i = 0; i < checkboxes.length; i++) {
		if (checkboxes[i].checked) {
			checked.push(checkboxes[i]);
		}
	}

	return checked;
}

function collectCheckboxes() {
	var checkboxes = document.getElementsByTagName("input");

	return checkboxes;
}

function deleteTerm() {

	var client = rest.chain(csrf, {
		token: $("meta[name='_csrf']").attr("content"),
		});
	
	var list = collectCheckboxes();
	var checked = collectCheckedCheckboxes(list);

	if (checked.length == 0) {
		alert("Select a term");
	} else {
		var res = "list=";
		var token = client;

		for (var i = 0; i < checked.length; i++) {
			if (i + 1 == checked.length) {
				res += checked[i].attributes.id.value;
			} else {
				res += checked[i].attributes.id.value + ",";
			}
		}

		res += "&_csrf=" + token;

		console.log(res);

		var xmlHttp = new XMLHttpRequest();
		xmlHttp.open("POST", "termDelete", true);
		xmlHttp.setRequestHeader("Content-type",
				"application/x-www-form-urlencoded");
		xmlHttp.send(res);

		tableClear(checked);

		return xmlHttp.responseText;
	}
}

function tableClear(checkedCheckboxes) {
	if (checkedCheckboxes.length == 0) {
		alert("Something terrible happend");
	} else {
		var table = checkedCheckboxes[0].parentNode.parentNode.parentNode;
		for (var i = 0; i < checkedCheckboxes.length; i++) {
			var tr = checkedCheckboxes[i].parentNode.parentNode;
			table.removeChild(tr)
		}
	}
}