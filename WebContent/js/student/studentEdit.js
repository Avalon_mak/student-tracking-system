function goToList() {
	window.location = 'studentList';
}

function validator(context, field) {
	alert('enter');
	for (var i = 0; i < context.messages.length; i++) {
		var temp = context.messages[i];
		if (temp.code == field) {
			return 'Field are empty';
		}
	}
	return '';
}

function checkData(tag) {
	var value = tag.value;

	if (value > 100) {
		tag.value = 100;
	} else if (value < 0) {
		tag.value = 0;
	}
}

function currentTermDisciplinesWithInput() {

	var client = rest.chain(csrf, {
		token: $("meta[name='_csrf']").attr("content"),
		});
	
	var xmlHttp = new XMLHttpRequest();
	var terms = document.getElementById("currentTerm");
	for (var i = 0; i < terms.length; i++) {
		if (terms[i].selected) {
			var currentTerm = terms[i];
		}
	}

	var currentId = currentTerm.value;

	var res = "list=" + currentId;
	var studId = "&studId=" + document.getElementById("id").value;
	var token = client;

	res += studId + "&_csrf=" + token;

	xmlHttp.open("POST", "termChange", true);
	xmlHttp.setRequestHeader("Content-type",
			"application/x-www-form-urlencoded");
	xmlHttp.onreadystatechange = function() {
		if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
			tableRepaintWithInput(xmlHttp.responseText);
		}
	};
	xmlHttp.send(res);
}

function tableRepaintWithInput(responseText) {
	var table = document.getElementById("disciplinesTable");
	while (table.firstChild) {
		table.removeChild(table.firstChild);
	}
	var result = JSON.parse(responseText);
	for (var i = 0; i < result.disciplines.length; i++) {
		var dis = result.disciplines[i];
		table.insertRow(i).insertCell(0).innerHTML = '<input type="hidden" name="disciplineId" value="'
				+ dis.id
				+ '"><label>'
				+ dis.name
				+ ':<input name="rating" type="number" onchange="javascript:checkData(this)" value="'
				+ dis.rating + '" />';
	}
}

function parseJSON(unparseJSON) {
	var json = JSON.parse(unparseJSON);
	return json;
}

function doSomething() {
}