function goToStudent(id) {

	window.location = 'studentProgress?id=' + id;

}

function goTo(action){
	var currentPage = document.getElementById("currentPage").value;
	var pagesCount = document.getElementById("pagesCount").value;
	
	if(currentPage > pagesCount || currentPage < 1 || action == "toFirst"){
		window.location = 'studentList?page=1';
	}
	
	if(action == "toPrev") {
		if (currentPage > 1) {
			currentPage--;
			window.location = 'studentList?page=' + currentPage;
		}
	} else if(action == "toLast") {
		window.location = 'studentList?page=' + pagesCount;
	} else if(action == "toNext") {
		currentPage++;
		window.location = 'studentList?page=' + currentPage;
	}
}

function goToStudentEdit() {

	var list = collectCheckboxes();

	var checked = collectCheckedCheckboxes(list);

	if (checked.length > 1) {

		alert("Take only ONE student");

	} else if (checked.length == 0) {

		alert("Select a student");

	} else {

		window.location = 'studentEdit?id='
				+ checked[0].attributes.studId.value;

	}

}

function collectCheckedCheckboxes(checkboxes) {

	var checked = [];

	for (var i = 0; i < checkboxes.length; i++) {

		if (checkboxes[i].checked) {

			checked.push(checkboxes[i]);

		}

	}

	return checked;

}

function collectCheckboxes() {

	var checkboxes = document.getElementsByTagName("input");

	return checkboxes;

}

function deleteStudent() {

	var list = collectCheckboxes();

	var meta = document.getElementsByName("_csrf")[0].getAttribute("content"); 
	
	var checked = collectCheckedCheckboxes(list);

	if (checked.length == 0) {

		alert("Select a student");

	} else {
		var res = "list=";
		var token = meta;
		
		for (var i = 0; i < checked.length; i++) {
			if (i + 1 == checked.length) {
				res += checked[i].attributes.studId.value;
			} else {
				res += checked[i].attributes.studId.value + ",";
			}
		}

		res += "&_csrf=" + token;

		console.log(res);

		var xmlHttp = new XMLHttpRequest();
		xmlHttp.open("POST", "studentDelete", true);
		xmlHttp.setRequestHeader("Content-type",
				"application/x-www-form-urlencoded");
		xmlHttp.send(res);
		tableClear(checked);
		
		return xmlHttp.responseText;

	}

}

function tableClear(checkedCheckboxes) {

	if (checkedCheckboxes.length == 0) {

		alert("Something terrible happend");

	} else {

		var table = checkedCheckboxes[0].parentNode.parentNode.parentNode;

		for (var i = 0; i < checkedCheckboxes.length; i++) {

			var tr = checkedCheckboxes[i].parentNode.parentNode;

			table.removeChild(tr)

		}

	}
}