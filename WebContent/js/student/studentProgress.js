function goToStudentEdit(id) {
	window.location = 'studentEdit?id='
			+ id;

}

function currentTermDisciplines() {

	var client = rest.chain(csrf, {
		token: $("meta[name='_csrf']").attr("content"),
		});
	
	var xmlHttp = new XMLHttpRequest();
	var terms = document.getElementById("currentTerm");
	for (var i = 0; i < terms.length; i++) {
		if (terms[i].selected) {
			var currentTerm = terms[i];
		}
	}

	var currentId = currentTerm.value;

	var res = "list=" + currentId;
	var studId = "&studId=" + document.getElementById("id").value;
	var token = client;

	res += studId + "&_csrf=" + token;

	xmlHttp.open("POST", "termChange", true);
	xmlHttp.setRequestHeader("Content-type",
			"application/x-www-form-urlencoded");
	xmlHttp.onreadystatechange = function() {
		if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
			tableRepaint(xmlHttp.responseText, token);
		}
	};
	xmlHttp.send(res);
}

function tableRepaint(responseText, csrf) {
	var table = document.getElementById("disciplinesTable");
	while (table.firstChild) {
		table.removeChild(table.firstChild);
	}
	var result = JSON.parse(responseText);
	var ratings=[result.disciplines.length];
	for (var i = 0; i < result.disciplines.length; i++) {
		var dis = result.disciplines[i];
		table.insertRow(i).insertCell(0).innerHTML = '<input type="hidden" name="disciplineId" value="'
				+ dis.id
				+ '"><label name="disciplinesNames">'
				+ dis.name
				+ ':</label>' + dis.rating;
		ratings[i] = dis.rating;
	}
	changeAverage(ratings, client);
}

function changeAverage(ratings, csrf){
	var xmlHttp = new XMLHttpRequest();
	var averageField = document.getElementById("average");
	var newAverage = 0;
	
	var res = "rates=" + ratings;
	res += "&_csrf=" + csrf;
	
	xmlHttp.open("POST", "averageChange", true);
	xmlHttp.setRequestHeader("Content-type",
			"application/x-www-form-urlencoded");
	xmlHttp.onreadystatechange = function() {
		if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
			averageField.textContent = "Средний бал: " + xmlHttp.responseText;
		}
	};
	xmlHttp.send(res);
}