//<![CDATA[

function goTo(action){
	
	var currentPage = document.getElementById("currentPage").value;
	var pagesCount = document.getElementById("pagesCount").value;
	
	if(currentPage > pagesCount || currentPage < 1 || action == "toFirst"){
		window.location = 'disciplineList?page=1';
	}
	
	if(action == "toPrev") {
		if (currentPage > 1) {
			currentPage--;
			window.location = 'disciplineList?page=' + currentPage;
		}
	} else if(action == "toLast") {
		window.location = 'disciplineList?page=' + pagesCount;
	} else if(action == "toNext") {
		currentPage++;
		window.location = 'disciplineList?page=' + currentPage;
	}
}

function tableRepaint(responseText, csrf, action){
	var currentDiscipline = [];
	currentDiscipline = document.getElementsByName("discipline");
	var table = currentDiscipline[0].parentNode;
	while (true) {
		table.removeChild(currentDiscipline[0]);
		if(currentDiscipline.length == 0){
			break;
		}
	}
	
	var result = JSON.parse(responseText);
	
	for (var i = 0; i < result.discipline.length; i++) {
		var discip = result.discipline[i];
		var row = table.insertRow(i+1);
		row.setAttribute("name", "student");
		row.insertCell(0).innerHTML = '<security:authorize access="hasRole(' + 'ROLE_ADMIN' + ')">'
		 + '<td class="table-bordered"><input type="checkbox"' + 'discId='+ discip.id + '/></td></security:authorize>';
		row.insertCell(1).innerHTML = '<td>' + discip.disciplineName + '</td>';	
	}
	
	
	var pageInput = document.getElementById("currentPage");
	
	if(action == "toNext"){
		pageInput.value++;
	}
	if(action == "toFirst") {
		pageInput.value = 1;
	}
	if(action == "toPrev" && pageInput.value > 1) {
		pageInput.value--;
	}
	if(action == "toLast" && pageInput.value > 1) {
		pageInput.value = document.getElementById("pagesCount").value;
	}
	
	
	var paging = document.getElementById("paging");
	
}

function goToDisciplineEdit() {
	var list = collectCheckboxes();
	var checked = collectCheckedCheckboxes(list);

	if (checked.length > 1) {
		alert("Take only ONE discipline");
	} else if (checked.length == 0) {
		alert("Select a discipline");
	} else {
		window.location = 'disciplineEdit?id='
				+ checked[0].attributes.discId.value;
	}
}

function collectCheckedCheckboxes(checkboxes) {
	var checked = [];
	for (var i = 0; i < checkboxes.length; i++) {
		if (checkboxes[i].checked) {
			checked.push(checkboxes[i]);
		}
	}

	return checked;
}

function collectCheckboxes() {
	var checkboxes = document.getElementsByTagName("input");

	return checkboxes;
}

function deleteDiscipline() {
	
	var meta = document.getElementsByName("_csrf")[0].getAttribute("content"); 
	

	var list = collectCheckboxes();
	var checked = collectCheckedCheckboxes(list);

	if (checked.length == 0) {
		alert("Select a discipline");
	} else {
		var res = "list=";
		var token = meta;

		for (var i = 0; i < checked.length; i++) {
			if (i + 1 == checked.length) {
				res += checked[i].attributes.discId.value;
			} else {
				res += checked[i].attributes.discId.value + ",";
			}
		}

		res += "&_csrf=" + token;

		console.log(res);

		var xmlHttp = new XMLHttpRequest();
		xmlHttp.open("POST", "disciplineDelete", true);
		xmlHttp.setRequestHeader("Content-type",
				"application/x-www-form-urlencoded");
		xmlHttp.send(res);

		tableClear(checked);

		return xmlHttp.responseText;
	}
}

function tableClear(checkedCheckboxes) {
	if (checkedCheckboxes.length == 0) {
		alert("Something terrible happend");
	} else {
		var table = checkedCheckboxes[0].parentNode.parentNode.parentNode;
		for (var i = 0; i < checkedCheckboxes.length; i++) {
			var tr = checkedCheckboxes[i].parentNode.parentNode;
			table.removeChild(tr)
		}
	}
}
// ]]>
