<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles-extras"
	prefix="tilesx"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1" />

<meta name="_csrf" content="${_csrf.token}"/>
<!-- default header name is X-CSRF-TOKEN -->
<meta name="_csrf_header" content="${_csrf.headerName}"/>

<title><tiles:getAsString name="title" /></title>

<!-- Bootstrap Core CSS -->
<link
	href="${pageContext.request.contextPath}/css/bootstrap/bootstrap.css"
	rel="stylesheet" />

<!-- Custom CSS -->
<link href="${pageContext.request.contextPath}/css/custom.css"
	rel="stylesheet" />
</head>

<body>
	<!-- Header -->
	<nav class="navbar navbar-inverse navbar-fixed-top">
	<div class="container-fluid">
		<div class="collapse navbar-collapse"
			id="bs-example-navbar-collapse-1">
			<div class="row">
				<div class="col-md-4"></div>
				<div class="col-md-4">
					<div class="container-fliud header-container">
						<h3><spring:message code="general.title"/></h3>
					</div>
					<div class="col-md-4"></div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 breedcrumbs-panel">
					<tilesx:useAttribute name="hideMenu" id="menu" />
					<c:if test="${not menu}">
						<a href="${pageContext.request.contextPath}/app/home"
							class="breedcrumbs-button">Главня</a>
						<c:forEach items="${breedcrumds}" var="nav" varStatus="index">
							<a href="${pageContext.request.contextPath}${nav}"
								class="prev-page breedcrumbs-button"><spring:message code="${navNames[index.index]}"/></a>
						</c:forEach>
						<span class="current-page breedcrumbs-button"><tiles:getAsString name="title" /></span>
					</c:if>

				</div>
				<div class="col-md-4"></div>
				<div class="col-md-3">
					<span class="greetings"><h3><spring:message code="general.greetings"/> ${username}</h3></span>
				</div>
				<div class="col-md-1">
					<span class=logout-button><a
						href="${pageContext.request.contextPath}/login?logout"
						class="navbar-brand custom-float-left"><spring:message code='general.logout'/></a></span>
				</div>
			</div>
		</div>
	</div>
	</div>
	</nav>

	<!-- Page Content -->
	<div class="row content-row">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-2"></div>
				<div class="col-md-8 general-container">
					<tiles:insertAttribute name="body" />
				</div>
				<div class="col-md-2 text-center"></div>
			</div>
		</div>
	</div>
	<!-- /.container -->
</body>
</html>
