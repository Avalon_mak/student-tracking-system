<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="security"%>
	<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<head>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/student/studentList.js"></script>
</head>
<h3><spring:message code="student.listTitle" /></h3>
<form:form action="studentList">
	<div class="row">
		<span><spring:message code="search.text" />:</span>
		<input name="searchInput" id="searchInput"/>
		<select name="filter" id="searchFilter">
			<option value="name"><spring:message code="searchSelectorStudent.name" /></option>
			<option value="group"><spring:message code="searchSelector.group" /></option>
			<option value="date"><spring:message code="searchSelector.date" /></option>
		</select>
		<input type="submit" class="btn btn-default" id="searchButton" value =<spring:message code="searchButton.name" /> />
	</div>
</form:form>
<br />
<div class="container-fluid">
	<security:authorize access="hasRole('ROLE_ADMIN')">
		<a href="studentCreate" class="btn btn-default"><spring:message code="button.add" /></a>
		<a onclick="javascript:deleteStudent()"
			class="btn btn-default"><spring:message code="button.delete" /></a>
	</security:authorize>

	<table class="table table-hover">
		<tr>
			<security:authorize access="hasRole('ROLE_ADMIN')">
				<th></th>
			</security:authorize>
			<th><spring:message code="table.secName" /></th>
			<th><spring:message code="table.firstName" /></th>
			<th><spring:message code="table.group" /></th>
			<th><spring:message code="table.date" /></th>
		</tr>
		<c:forEach items="${studentList}" var="stud">
			<tr name="student">
				<security:authorize access="hasRole('ROLE_ADMIN')">
					<td class="table-bordered"><input type="checkbox"
						studId="${stud.id}" /></td>
				</security:authorize>
				<td onclick="goToStudent(${stud.id})">${stud.lastName}</td>
				<td onclick="goToStudent(${stud.id})">${stud.name}</td>
				<td onclick="goToStudent(${stud.id})">${stud.universityGroup}</td>
				<td onclick="goToStudent(${stud.id})">${stud.admissionDate}</td>
			</tr>
		</c:forEach>

	</table>
	<div class="row">
		<div class="container-fluid">
			<div class="col-xs-4"></div>
			<div class="col-xs-4 paging" id="paging">
				<span id="prevPage" onclick="goTo('toPrev')"><</span>
				<span id="firstPage" onclick="goTo('toFirst')"><<</span>
				<input id="currentPage" value="${currentPage}"
					onkeypress="goToSelectedPage()"></input>
				<span>/${pages}</span>
				 <input type="hidden" id="pagesCount" value="${pages}">
				<span id="lastPage" onclick="goTo('toLast')">>></span>
				<span id="nextPage" onclick="goTo('toNext')">></span>
			</div>
			<div class="col-xs-4"></div>
		</div>
	</div>
</div>
