<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://makassasin.com/custom" prefix="m"%>

<head>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/student/studentEdit.js"></script>
</head>

<h3>
	<spring:message code="${title}" />
</h3>
<br />
<div class="container-fluid">
	<form:form action="${url}" modelAttribute="student" method="POST">
		<table>
			<form:hidden path="id" />
			<tr>
				<td><form:label path="lastName"><spring:message code="table.secName" />:</form:label></td>
				<td><form:input id="forText" path="lastName"></form:input>
					<m:error code="${messages}" field="lastName" /></td>
			</tr>
			<tr>
				<td><form:label path="name"><spring:message code="table.firstName" />:</form:label></td>
				<td><form:input id="forText" path="name"></form:input>
					<m:error code="${messages}" field="name" /></td>
			</tr>
			<tr>
				<td><form:label path="universityGroup"><spring:message code="table.group" />:</form:label></td>
				<td><form:input id="forText" path="universityGroup"></form:input>
					<m:error code="${messages}" field="universityGroup" /></td>
			</tr>
			<tr>
				<td><form:label path="admissionDate"><spring:message code="table.date" />:</form:label></td>
				<td><form:input id="forText" path="admissionDate"></form:input>
					<m:error code="${messages}" field="admissionDate" /></td>
			</tr>
		</table>
		<select id="currentTerm" name="currentTerm"
			onchange="currentTermDisciplinesWithInput()">
			<c:if test="${terms != null}">
				<c:forEach items="${terms}" var="term">
					<option value="${term.id}">${term.termName}</option>
				</c:forEach>
			</c:if>
			<c:if test="${terms == null}">
				<option value=""><spring:message code="table.termsList" /></option>
			</c:if>
		</select> <m:error code="${messages}" field="currentTerm" />
		<table id="disciplinesTable" class="table table-hover">
		</table>
		<input class="btn btn-default" type="submit" value="<spring:message code="button.submit" />"
			onclick="goToList()" />
	</form:form>
	<script type="text/javascript">
	function disciplinesTable(DTOJson, context){
		var table = document.getElementById("disciplinesTable");

		for(var i = 0; i < DTOJson.disciplines.length; i++){
			var dis = DTOJson.disciplines[i];
			addElement(dis, i, context);
		}
	}

	function addElement(discipline, position, context) {
		var table = document.getElementById("disciplinesTable");
		var newTr = document.createElement("tr");
		var newTd = document.createElement("td");
		var newTd2 = document.createElement("td");
		var newInput = document.createElement("input");
		var newLabel = document.createElement("label");
		var inputRate = document.createElement("input");
		var textNode = document.createTextNode(discipline.name + ':');
		newLabel.appendChild(textNode);
		newTd.setAttribute('id', 'disciplineTableTd');
		newInput.setAttribute('type', 'hidden');
		newInput.setAttribute('name', 'disciplineId');
		newInput.setAttribute('value', discipline.id);
		inputRate.setAttribute('id', 'rating' + position);
		inputRate.setAttribute('name', 'rating' + position);
		inputRate.setAttribute('type', 'number');
		inputRate.setAttribute('onchange', 'javascript:checkData(this)');
		inputRate.setAttribute('value', discipline.rating);


		newTd.appendChild(newLabel);
		newTd2.appendChild(inputRate);
		newTd2.appendChild(newInput);

		if(context.messages != null){
			var res = validator(context, 'rating' + position);
			if(res != ''){
				alert('test');
				var errorNode = document.createTextNode(res);
				newTd.appendChild(errorNode);
			}
		}

		newTr.appendChild(newTd);
		newTr.appendChild(newTd2);
		table.appendChild(newTr);
	}

	disciplinesTable(${disciplines}, ${validationContext});
	</script>
</div>
