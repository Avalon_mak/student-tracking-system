<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="security"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<head>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/student/studentProgress.js"></script>
</head>

<h3><spring:message code='student.progressTitle'/></h3>
<br />
<div class="container-fluid">
	<table class="table table-hover">
		<th>${student.lastName} ${student.name}</th>
		<tr>
			<input type="hidden" value='${student.id}' id="id" />
		<tr>
			<td><spring:message code='table.secName'/>: ${student.lastName}</td>
		</tr>
		<tr>
			<td><spring:message code='table.firstName'/>: ${student.name}</td>
		</tr>
		<tr>
			<td><spring:message code='table.group'/>: ${student.universityGroup}</td>
		</tr>
		<tr>
			<td><spring:message code='table.date'/>: ${student.admissionDate}</td>
		</tr>
		<tr>
			<td id="average"><spring:message code='table.average'/>: ${average}</td>
		</tr>
		</tr>
		<b><tr>
				<td><spring:message code='table.progress'/>:</td>
			</tr></b>
	</table>
	<table class="table table-hover" id="disciplinesTable">
		<select id="currentTerm" name="currentTerm"
			onchange="currentTermDisciplines()">
			<c:if test="${terms != null}">
				<c:forEach items="${terms}" var="term">
					<option value="${term.id}">${term.termName}</option>
				</c:forEach>
			</c:if>
			<c:if test="${terms == null}">
				<option><spring:message code='table.termsList'/></option>
			</c:if>
		</select>
		</td>
		</tr>
		</b>
		<c:forEach items="${disciplines}" var="disc" varStatus="pos">
			<tr>
				<td><Label>${disc.disciplineName}:</label>${rate[pos.index]}</td>
			</tr>
		</c:forEach>
	</table>
	<a onclick="javascript:goToStudentEdit('${student.id}')" class="btn btn-default"><spring:message code='button.edit'/></a>
</div>
