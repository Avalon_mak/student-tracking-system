<%@ page contentType="text/html; charset=UTF-8" %>

<h3>Главное меню</h3>
<br/>
<div class="row">
    <div class="col-md-4 link" id="students-link">
        <a class="btn btn-link btn-lg" href="studentList">Студенты</a>
    </div>
    <div class="col-md-4 link" id="disciplines-link">
        <a class="btn btn-link btn-lg" href="disciplineList">Дисциплины</a>
    </div>
    <div class="col-md-4 link" id="semesters-link">
        <a class="btn btn-link btn-lg" href="termList">Семестры</a>
    </div>
</div>
