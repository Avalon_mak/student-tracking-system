<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://makassasin.com/custom" prefix="m" %>

<head>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/discipline/disciplineEdit.js"></script>
</head>

<h3><spring:message code="${title}"/></h3>
<br/>
<div class="container-fluid">
	<form:form action="${url}" modelAttribute="discipline" method="POST">
		<table>
			<form:hidden path="id"/>
			<tr>
				<td><form:label path="disciplineName"><spring:message code="table.discipline"/>:</form:label></td>
				<td><form:input path="disciplineName"></form:input><m:error code="${messages}" field="disciplineName" /></td>
			</tr>
			<tr>
				<td colspan="2">
					<input class="btn btn-default" type="submit" value='<spring:message code="button.submit"/>' onclick="goToList()"/>
				</td>
			</tr>
		</table>
	</form:form>
</div>
