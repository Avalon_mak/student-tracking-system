<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<head>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/term/termEdit.js"></script>
</head>

<h3><spring:message code="${title}"/></h3>
<br/>
<div class="container-fluid">
	<form:form action="${url}" modelAttribute="term" method="POST">
		<table>
			<form:hidden path="id"/>
			<input type="hidden" name="selectedDisciplines" id="selectedDisciplines"/>
			<tr>
				<td><form:label path="termName"><spring:message code='term.term'/>:</form:label></td>
				<td><form:input path="termName"></form:input></td>
			</tr>

			<tr>

				<td style='width:160px;'>
					<b><spring:message code='term.avalibleDisciplines'/>:</b></br>
					<select multiple="multiple" id="availableDisciplinesList">
						<c:forEach items="${disciplines}" var="disc">
							<option value="${disc.id}">${disc.disciplineName}</option>
						</c:forEach>
					</select>
				</td>
	
				<td style='width:50px;text-align:center;vertical-align:middle;'>
					<input type='button' id='btnRight' onclick="javascript:selectDiscipline()" value ='  >  '/>
					<br/><input type='button' id='btnLeft' onclick="javascript:unselectDiscipline()" value ='  <  '/>
				</td>
	
				<td style='width:160px;'>
					<b><spring:message code='term.selectedDisciplines'/>:</b><br />
					<select multiple="multiple" id="selectedDisciplinesList">
						<c:forEach items="${selectedDisciplines}" var="sDisc">
							<option value="${sDisc.id}">${sDisc.disciplineName}</option>
						</c:forEach>
					</select>
				</td>
			</tr>

			<tr>
				<td colspan="2">
					<input class="btn btn-default" type="submit" value="<spring:message code='button.submit'/>" onclick="goToList()" />
				</td>
			</tr>
        </table>
    </form:form>
</div>
