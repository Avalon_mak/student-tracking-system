<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<head>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/term/termList.js"></script>
</head>

<h3><spring:message code="term.title"/></h3>
<form:form action="${url}">
<div class="row">
	<sapn><spring:message code="search.text" />:</sapn>
	<input name="searchInput" id="searchInput" />
	<select name="filter" id="searchFilter">
		<option value="name"><spring:message code="searchSelector.name" /></option>
	</select>
	<span class="btn btn-default" id="searchButton" onclick="submitSearch()"><spring:message code="searchButton.name" /></span>
</div>
</form:form>
<br/>
<div class="container-fluid">
	<security:authorize access="hasRole('ROLE_ADMIN')">
		<a href="termCreate" class="btn btn-default"><spring:message code="button.add"/></a>
		<a
			onclick="javascript:goToTermEdit()"
			class="btn btn-default"><spring:message code="button.edit"/></a>
		<a
			onclick="javascript:deleteTerm('${_csrf.token}')"
			class="btn btn-default"><spring:message code="button.delete"/></a>
	</security:authorize>
	<table class="table table-hover">
		<tr>
			<security:authorize access="hasRole('ROLE_ADMIN')">
				<th></th>
			</security:authorize>
			<th><spring:message code="term.number"/></th>
		</tr>
		<c:forEach items="${termsList}" var="term">
			<tr name = "term">
				<security:authorize access="hasRole('ROLE_ADMIN')">
					<td class="table-bordered"><input type="checkbox"
						id="${term.id}" /></td>
				</security:authorize>
				<td onclick="javascript:goToTermDetails(${term.id})">${term.termName}</td>
			</tr>
		</c:forEach>
	</table>
		<div class="row">
		<div class="container-fluid">
			<div class="col-xs-4"></div>
			<div class="col-xs-4 paging" id="paging">
				<span id="firstPage" onclick="goTo('toFirst')"><<</span> <span
					id="prevPage" onclick="goTo('toPrev')"><</span> <input
					id="currentPage" value="${currentPage}"
					onkeypress="goToSelectedPage()"></input><span id="pagesCount"
					value='${pages}'>/${pages}</span> <span id="lastPage"
					onclick="goTo('toLast')">>></span> <span id="nextPage"
					onclick="goTo('toNext')">></span>
			</div>
			<div class="col-xs-4"></div>
		</div>
	</div>
</div>
