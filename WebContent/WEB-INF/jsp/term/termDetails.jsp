<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<h3><spring:message code='terms.detailsTitle'/></h3>
<br />
<div class="container-fluid">
	<table class="table table-hover">
		<th>${term.termName}</th>
		<tr>
			<c:forEach items="${term.disciplines}" var="discipline">
				<tr><td>${discipline}</td></tr>
			</c:forEach>
		</tr>
	</table>
</div>
