package com.students.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import com.students.data.discipline.model.Discipline;
import com.students.data.discipline.service.DisciplineService;
import com.students.data.rating.domain.RatingDTO;
import com.students.data.rating.model.Rating;
import com.students.data.student.model.Student;
import com.students.data.rating.service.RatingService;
import com.students.data.term.model.Term;
import com.students.data.term.service.TermService;
import com.students.utils.BreedcrumbsConfig;
import com.students.utils.BreedcrumdsWithId;
import com.students.utils.ValidationContext;
import com.students.utils.ValidationMessage;
import com.students.utils.Validator;

import org.apache.commons.collections.map.MultiValueMap;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import com.students.data.student.domain.StudentDTO;
import com.students.data.student.service.StudentService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class StudentController {

	private static final Logger LOGGER = LoggerFactory.getLogger(StudentController.class);

	private static final SimpleDateFormat FORMATTER = new SimpleDateFormat("dd/MM/yyyy");

	@Autowired
	private BreedcrumbsConfig breedcrumbsConfig;

	@Autowired
	private StudentService studentService;

	@Autowired
	private TermService termService;

	@Autowired
	private DisciplineService disciplineService;

	@Autowired
	private RatingService ratingService;


	@RequestMapping("/studentList")
	public ModelAndView studentList(@RequestParam Map<String, String> allRequestParams, ModelMap model) {

		List<Student> students = new ArrayList<Student>();
		List<StudentDTO> studentDTOList = new ArrayList<StudentDTO>();

		if (allRequestParams.size() != 0 && allRequestParams.get("searchInput") != null) {
			String filter = allRequestParams.get("filter");
			String[] forSearch = allRequestParams.get("searchInput").split(" ");
			students = toSearch(forSearch, filter);
		} else {
			students = studentService.listAll();
		}

		String page = allRequestParams.get("page");
		Map<String, Integer> pages = pagination(students, page);

		for (int i = pages.get("start"); i < pages.get("end"); i++) {
			StudentDTO studentDTO = new StudentDTO();
			studentDTO.setId(String.valueOf(students.get(i).getId()));
			studentDTO.setName(students.get(i).getName());
			studentDTO.setLastName(students.get(i).getLastName());
			studentDTO.setUniversityGroup(students.get(i).getUniversityGroup());
			if (students.get(i).getAdmissionDate() != null) {
				studentDTO.setAdmissionDate(FORMATTER.format(students.get(i).getAdmissionDate()));
			}
			studentDTOList.add(studentDTO);
		}

		return createMVForList("studentList", "app.studentList", studentDTOList, pages.get("currentPage"),
				pages.get("pagesCount"));
	}

	public List<Student> toSearch(String[] searchBody, String filter) {
		List<Student> students = new ArrayList<Student>();
		if (filter.equals("name")) {
			String name = searchBody[0];
			String lastName = searchBody[1];
			students = this.studentService.searchByName(name, lastName);
		} else if (filter.equals("group")) {
			String group = searchBody[0];
			students = this.studentService.searchByGroup(group);
		} else if (filter.equals("date")) {
			String date = searchBody[0];
			students = this.studentService.searchByDate(date);
		}

		return students;
	}

	public Map<String, Integer> pagination(List<Student> students, String page) {
		int start = 0;
		int end = 0;
		String currentPage = page;
		Map<String, Integer> result = new HashMap<String, Integer>();

		int pages = 0;
		if (students.size() > 10) {
			pages = students.size() / 10 + 1;
		} else {
			pages = 1;
		}

		if (students.size() < 10 || page == null) {
			currentPage = "1";
		} else {
			Integer size = students.size();
		}

		if (Integer.parseInt(currentPage) > 1 && Integer.parseInt(currentPage) <= pages) {
			end = 10 * Integer.parseInt(currentPage);
			start = end - 10;
			if (end > students.size()) {
				end = students.size();
			}
		} else {
			currentPage = "1";
			end = 10;
			if (end > students.size()) {
				end = students.size();
			}
		}

		result.put("pagesCount", pages);
		result.put("start", start);
		result.put("end", end);
		result.put("currentPage", Integer.parseInt(currentPage));

		return result;
	}

	public ModelAndView createMVForList(String url, String viewName, List<StudentDTO> students, Integer currentPage,
			Integer pagesCount) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String username = auth.getName();

		ModelAndView mv = new ModelAndView();
		mv.setViewName(viewName);
		mv.addObject("username", username);
		mv.addObject("url", url);
		mv.addObject("studentList", students);
		mv.addObject("currentPage", currentPage);
		mv.addObject("pages", pagesCount);

		return mv;
	}

	@RequestMapping("/studentProgress")
	public ModelAndView studentView(@RequestParam("id") Long id) {
		StudentDTO studentDTO = new StudentDTO();

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String username = auth.getName();

		ModelAndView mv = new ModelAndView();
		mv.setViewName("app.studentProgress");
		mv.addObject("username", username);

		Student student = studentService.get(id);
		studentDTO.setId(String.valueOf(student.getId()));
		studentDTO.setName(student.getName());
		studentDTO.setLastName(student.getLastName());
		studentDTO.setUniversityGroup(student.getUniversityGroup());

		if (student.getAdmissionDate() != null) {
			studentDTO.setAdmissionDate(FORMATTER.format(student.getAdmissionDate()));
		}

		List<Term> terms = termService.listAll();

		if (terms.size() > 0) {
			List<Rating> ratingsList = ratingService.getByTermAndStudId(id, terms.get(0).getId());
			Integer[] ratingsArray = new Integer[ratingsList.size()];
			Discipline[] disciplines = new Discipline[ratingsList.size()];
			double average = 0;
			int counter = 0;
			for (Rating r : ratingsList) {
				disciplines[counter] = ratingsList.get(counter).getDiscipline();
				ratingsArray[counter] = r.getRating();
				average += r.getRating();
				counter++;
			}
			average /= counter;
			mv.addObject("disciplines", disciplines);
			mv.addObject("rate", ratingsArray);
			mv.addObject("average", average);
			mv.addObject("terms", terms);
		} else {
			mv.addObject("terms", null);
			mv.addObject("disciplines", null);
			mv.addObject("rate", null);
			mv.addObject("average", null);
		}

		Map<String, List<String>> breedcrumbsUrls = this.breedcrumbsConfig.getCodes("studentList");

		mv.addObject("breedcrumds", breedcrumbsUrls.get("url"));
		mv.addObject("navNames", breedcrumbsUrls.get("name"));

		mv.addObject("student", studentDTO);
		return mv;
	}

	@RequestMapping(value = "/studentCreate", method = RequestMethod.GET)
	public ModelAndView studentCreateGet() {
		StudentDTO studentDTO = new StudentDTO();
		studentDTO.setLastName("");
		studentDTO.setName("");
		studentDTO.setUniversityGroup("");
		studentDTO.setAdmissionDate("");

		return createUpdateGET(studentDTO);
	}

	public ModelAndView createUpdateGET(StudentDTO studentDTO) {
		RatingDTO ratingDTO = new RatingDTO();
		List<Term> termsList = termService.listAll();
		Map<String, Object> MVObjects = new HashMap<String, Object>();

		if (studentDTO.getId() == null) {
			MVObjects.put("viewName", "app.studentCreate");
			MVObjects.put("title", "student.create");
			MVObjects.put("url", "studentCreate");

			MVObjects.put("breedcrumbs", this.breedcrumbsConfig.getCodes("studentList"));
		} else {
			MVObjects.put("viewName", "app.studentEdit");
			MVObjects.put("title", "student.edit");
			MVObjects.put("url", "studentEdit");

			MVObjects.put("breedcrumbs", this.breedcrumbsConfig.getCodes("studentList.progress"));
		}
		ratingDTO = ratingDTOCreate(termsList, studentDTO.getId());

		JSONObject ratingJSON = convertToJSON(ratingDTO);

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String username = auth.getName();

		MVObjects.put("disciplines", ratingJSON);
		ValidationContext validationContext = new ValidationContext();
		MVObjects.put("student", studentDTO);

		if (termsList.size() != 0) {
			MVObjects.put("terms", termsList);
		} else {
			MVObjects.put("terms", null);
		}

		MVObjects.put("messages", validationContext);
		MVObjects.put("validationContext", validationJSON(validationContext).toString());

		return MVForCreateUpdateGET(MVObjects);
	}

	public ModelAndView MVForCreateUpdateGET(Map<String, Object> MVObjects) {
		ModelAndView mv = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String username = auth.getName();

		mv.setViewName((String) MVObjects.get("viewName"));
		mv.addObject("title", MVObjects.get("title"));
		mv.addObject("url", MVObjects.get("url"));
		mv.addObject("title", MVObjects.get("title"));
		mv.addObject("username", username);

		Map<String, List<String>> links = (Map<String, List<String>>) MVObjects.get("breedcrumbs");
		if (MVObjects.get("id") != null) {
			List<String> resultBreedcrumbs = new BreedcrumdsWithId().breedcrumb(links, (String) MVObjects.get("id"));
			mv.addObject("breedcrumbs", resultBreedcrumbs);
			mv.addObject("navNames", links.get("name"));
		} else {
			mv.addObject("breedcrumbs", links.get("url"));
			mv.addObject("navNames", links.get("name"));
		}

		mv.addObject("disciplines", MVObjects.get("disciplines"));
		mv.addObject("student", MVObjects.get("student"));
		mv.addObject("terms", MVObjects.get("terms"));
		mv.addObject("messages", MVObjects.get("messages"));
		mv.addObject("validationContext", MVObjects.get("validationContext"));

		return mv;
	}

	public RatingDTO ratingDTOCreate(List<Term> terms, String id) {
		Set<Discipline> disciplinesSet;
		List<Rating> ratingsList = new ArrayList<Rating>();
		List<String> names = new ArrayList<String>();
		List<String> ids = new ArrayList<String>();
		List<String> ratingsCounts = new ArrayList<String>();
		RatingDTO ratingDTO = new RatingDTO();

		if (terms.size() == 0) {
			disciplinesSet = new HashSet<Discipline>();
		} else {
			disciplinesSet = terms.get(0).getDisciplines();
			if (id != null) {
				ratingsList = ratingService.getByTermAndStudId(Long.parseLong(id), terms.get(0).getId());
				for (Rating r : ratingsList) {
					Iterator<Discipline> iterator = disciplinesSet.iterator();
					while (iterator.hasNext()) {
						if (iterator.next().getDisciplineName().equals(r.getDiscipline().getDisciplineName())) {
							iterator.remove();
							break;
						}
					}
					names.add(r.getDiscipline().getDisciplineName());
					ids.add(String.valueOf(r.getDiscipline().getId()));
					ratingsCounts.add(String.valueOf(r.getRating()));
				}
				ratingDTO.setRating(ratingsCounts);
			}
		}
		for (Discipline d : disciplinesSet) {
			ids.add(String.valueOf(d.getId()));
			names.add(d.getDisciplineName());
		}
		ratingDTO.setDisciplinesIds(ids);
		ratingDTO.setDisciplinesNames(names);

		return ratingDTO;
	}

	@RequestMapping(value = "/studentCreate", method = RequestMethod.POST)
	public ModelAndView studentCreatePost(HttpServletResponse response, HttpServletRequest request,
			@ModelAttribute("student") StudentDTO studentDTO, BindingResult bindingResult) {

		RatingDTO ratingDTO;
		List<String> ratingsList = new ArrayList<String>();
		String currentTerm = request.getParameter("currentTerm");
		String[] disciplines = (String[]) request.getParameterMap().get("disciplineId");
		
		Validator validation;
		if (disciplines != null) {
			for (int i = 0; i < disciplines.length; i++) {
				ratingsList.add(((String[]) request.getParameterMap().get("rating" + i))[0]);
			}

			List<String> disciplinesList = new ArrayList<String>();
			List<String> discN = new ArrayList<String>();

			for (int i = 0; i < disciplines.length; i++) {
				discN.add(disciplineService.get(Long.parseLong(disciplines[i])).getDisciplineName());
				disciplinesList.add(disciplines[i]);
			}

			ratingDTO =  ratingDTOforCreactePOST(disciplinesList, discN, currentTerm, ratingsList);
			
			validation = validationForCreatePOST(studentDTO, ratingDTO);

			List<Term> terms = termService.listAll();

			JSONObject ratingJSON = new JSONObject();
			if (ratingDTO.getId() != null) {
				ratingJSON = convertToJSON(ratingDTO);
			}
			JSONObject validationResultJSON = validationJSON(validation.getContext());

			if (validation.getContext().getMessages().size() > 0) {
				return MVForValidation("app.studentCreate", validation.getContext(), "student.create", studentDTO,
						"studentCreate", ratingJSON, validationResultJSON.toString(), terms);
			}
			return createUpdatePOST(studentDTO, ratingDTO);
		} else {
			ratingDTO =  ratingDTOforCreactePOST(null, null, currentTerm, ratingsList);
			
			validation = validationForCreatePOST(studentDTO, ratingDTO);

			List<Term> termsList = termService.listAll();

			JSONObject result = new JSONObject();
			if (ratingDTO.getId() != null) {
				result = convertToJSON(ratingDTO);
			}
			JSONObject validResult = validationJSON(validation.getContext());

			if (validation.getContext().getMessages().size() > 0) {
				return MVForValidation("app.studentCreate", validation.getContext(), "student.create", studentDTO,
						"studentCreate", result, validResult.toString(), null);
			}
		}

		return null;
	}
	
	public Validator validationForCreatePOST(StudentDTO studentDTO, RatingDTO ratingDTO){
		ValidationContext validationContext = new ValidationContext();
		Validator validator = new Validator();
		validator.setContext(validationContext);
		validator.validateStudent(studentDTO, ratingDTO);
		
		return validator;
	}
	
	public RatingDTO ratingDTOforCreactePOST(List<String> disciplinesList, List<String> discN, String currentTerm, List<String> ratingsList){
		RatingDTO ratingDTO = new RatingDTO();
		
		ratingDTO.setDisciplinesIds(disciplinesList);
		ratingDTO.setDisciplinesNames(discN);
		ratingDTO.setTerm(currentTerm);
		ratingDTO.setRating(ratingsList);
		
		return ratingDTO;
	}

	@RequestMapping(value = "/studentEdit", method = RequestMethod.GET)
	public ModelAndView studentEditGet(@RequestParam("id") Long id) {

		StudentDTO result = new StudentDTO();
		Student student = studentService.get(id);

		result.setLastName(student.getLastName());
		result.setId(student.getId().toString());
		result.setName(student.getName());
		result.setUniversityGroup(student.getUniversityGroup());
		if (student.getAdmissionDate() != null) {
			result.setAdmissionDate(FORMATTER.format(student.getAdmissionDate()));
		}
		return createUpdateGET(result);
	}

	@RequestMapping(value = "/studentEdit", method = RequestMethod.POST)
	public ModelAndView у(HttpServletRequest request, @ModelAttribute("student") StudentDTO studentDTO,
			BindingResult bindingResult) {

		RatingDTO ratingDTO = new RatingDTO();
		List<String> listNames = new ArrayList<String>();
		List<String> listIds = new ArrayList<String>();
		List<String> listRates = new ArrayList<String>();
		String currentTerm = request.getParameter("currentTerm");

		List<String> ratingsList = new ArrayList<String>();
		String[] ids = (String[]) request.getParameterMap().get("disciplineId");
		if (ids != null && ids.length > 0) {
			for (int i = 0; i < ids.length; i++) {
				ratingsList.add(((String[]) request.getParameterMap().get("rating" + i))[0]);
				listIds.add(ids[i]);
				listRates.add(ratingsList.get(i));
			}
		}

		
		ratingDTO = ratingDTOforCreactePOST(listIds, listNames, currentTerm, listRates);

		if (currentTerm != "") {
			List<Rating> ratings = ratingService.getByTermAndStudId(Long.parseLong(studentDTO.getId()),
					Long.parseLong(currentTerm));
			String[] ratingsIds = new String[ratings.size()];
			int counter = 0;
			for (Rating r : ratings) {
				ratingsIds[counter] = r.getId().toString();
				counter++;
			}
			ratingDTO.setId(ratingsIds);
		}

		List<ValidationMessage> messages;
		Validator validator = validationForCreatePOST(studentDTO, ratingDTO);
		messages = validator.validateStudent(studentDTO, ratingDTO);

		List<Term> termsList = termService.listAll();

		JSONObject ratingsJSON = convertToJSON(ratingDTO);

		if (messages.size() > 0) {
			LOGGER.info("All works fine");

			return MVForValidation("app.studentEdit", validator.getContext(), "student.edit", studentDTO, "studentEdit",
					convertToJSON(ratingDTO), validationJSON(validator.getContext()).toString(), termsList);
		}

		return createUpdatePOST(studentDTO, ratingDTO);
	}

	public ModelAndView MVForValidation(String viewName, ValidationContext validationContext, String title,
			StudentDTO studentDTO, String url, JSONObject disciplines, String context, List<Term> terms) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String username = auth.getName();

		LOGGER.info("All works fine");
		ModelAndView mv = new ModelAndView();

		mv.setViewName("app.studentEdit");

		mv.addObject("username", username);
		mv.addObject("title", title);
		mv.addObject("messages", validationContext);
		mv.addObject("url", url);
		mv.addObject("student", studentDTO);
		mv.addObject("disciplines", disciplines.toString());
		mv.addObject("validationContext", validationJSON(validationContext).toString());
		mv.addObject("rating", terms);

		Map<String, List<String>> urls = this.breedcrumbsConfig.getCodes("studentList");

		mv.addObject("breedcrumds", urls.get("url"));
		mv.addObject("navNames", urls.get("name"));

		return mv;
	}

	public ModelAndView createUpdatePOST(StudentDTO studentDTO, RatingDTO ratingDTO) {

		Student student = new Student();
		List<Rating> ratings = new ArrayList<Rating>();

		if (!studentDTO.getId().equals("")) {
			student.setId(Long.parseLong(studentDTO.getId()));
		}
		student.setName(studentDTO.getName());
		student.setLastName(studentDTO.getLastName());
		student.setUniversityGroup(studentDTO.getUniversityGroup());
		try {
			student.setAdmissionDate(FORMATTER.parse(studentDTO.getAdmissionDate()));
		} catch (ParseException e) {
			e.printStackTrace();
		}

		studentService.save(student);

		if (ratingDTO.getTerm() != null) {
			Rating rating;
			Term term = termService.get(Long.parseLong(ratingDTO.getTerm()));
			for (int i = 0; i < ratingDTO.getDisciplinesIds().size(); i++) {
				rating = new Rating();
				if (ratingDTO.getId() != null) {
					rating.setId(Long.parseLong(ratingDTO.getId()[i]));
				}
				rating.setStudent(student);
				rating.setDiscipline(disciplineService.get(Long.parseLong(ratingDTO.getDisciplinesIds().get(i))));
				rating.setRating(Integer.parseInt(ratingDTO.getRating().get(i)));
				rating.setTerm(term);
				ratingService.save(rating);
			}
		}

		ModelAndView mv = new ModelAndView();
		mv.setViewName("redirect:studentList");

		return mv;
	}

	@RequestMapping(value = "/termChange", method = RequestMethod.POST)
	public @ResponseBody byte[] termChange(HttpServletRequest request, HttpServletResponse response,
			@RequestParam("list") String termId, @RequestParam("studId") String studId) throws Exception {

		List<Rating> ratings = new ArrayList<Rating>();
		List<String> rates = new ArrayList<String>();
		List<String> names = new ArrayList<String>();
		List<String> ids = new ArrayList<String>();
		Set<Discipline> disciplines = new HashSet<Discipline>();
		RatingDTO ratingDTO = new RatingDTO();
		ratings = ratingService.getByTermAndStudId(Long.parseLong(studId), Long.parseLong(termId));
		ratingDTO = ratingForTermChange(ratings, studId, termId);

		JSONObject result = convertToJSON(ratingDTO);

		return result.toString().getBytes("UTF-8");
	}

	public RatingDTO ratingForTermChange(List<Rating> ratings, String studId, String termId) {
		List<String> rates = new ArrayList<String>();
		List<String> names = new ArrayList<String>();
		List<String> ids = new ArrayList<String>();
		RatingDTO ratingDTO = new RatingDTO();
		Set<Discipline> disciplines = new HashSet<Discipline>();
		if (ratings.size() > 0 && studId != null) {
			for (Rating r : ratings) {
				disciplines.add(r.getDiscipline());
				rates.add(String.valueOf(r.getRating()));
				names.add(r.getDiscipline().getDisciplineName());
				ids.add(String.valueOf(r.getDiscipline().getId()));
			}
		} else {
			disciplines = termService.get(Long.parseLong(termId)).getDisciplines();
			Iterator<Discipline> iterator = disciplines.iterator();
			for (int i = 0; i < disciplines.size(); i++) {
				Discipline tempDiscipline = iterator.next();
				names.add(tempDiscipline.getDisciplineName());
				ids.add(String.valueOf(tempDiscipline.getId()));
			}
		}

		ratingDTO.setTerm(termId);
		ratingDTO.setDisciplinesIds(ids);
		ratingDTO.setDisciplinesNames(names);
		ratingDTO.setRating(rates);
		return ratingDTO;
	}

	@RequestMapping(value = "/studentDelete", method = RequestMethod.POST)
	public String studentDeletePost(@RequestParam("list") String list) {

		String ids = "";

		String[] splitted = list.split(",");
		for (int i = 0; i < splitted.length; i++) {
			if (i < splitted.length - 1) {
				if (splitted[i].charAt(0) > 58 || splitted[i].charAt(0) < 47) {
					i++;
				} else {
					ids += splitted[i] + ",";
				}
			} else {
				ids += splitted[i];
			}
		}
		splitted = ids.split(",");

		ratingService.delete(ids);
		studentService.delete(ids);

		return "redirect:/app/studentList?page=1";
	}

	public JSONObject convertToJSON(RatingDTO ratingDTO) {

		JSONObject result = new JSONObject();
		JSONArray array = new JSONArray();
		for (int i = 0; i < ratingDTO.getDisciplinesNames().size(); i++) {
			JSONObject disciplineJSON = new JSONObject();
			try {
				if (ratingDTO.getRating() != null && ratingDTO.getRating().size() != 0) {
					disciplineJSON.put("id", ratingDTO.getDisciplinesIds().get(i));
					disciplineJSON.put("name", ratingDTO.getDisciplinesNames().get(i));
					disciplineJSON.put("rating", ratingDTO.getRating().get(i));
				} else {
					disciplineJSON.put("id", ratingDTO.getDisciplinesIds().get(i));
					disciplineJSON.put("name", ratingDTO.getDisciplinesNames().get(i));
					disciplineJSON.put("rating", "");
				}
				array.put(disciplineJSON);
			} catch (Exception e) {
				LOGGER.error("Exception was thrown during JSON data forming" + e.getMessage());
			}
		}

		try {
			result.put("disciplines", array);
		} catch (Exception e) {
			LOGGER.error("Exception was thrown during JSON array forming" + e.getMessage());
		}

		return result;
	}

	public JSONObject validationJSON(ValidationContext context) {

		if (context.getMessages().size() == 0) {
			return new JSONObject();
		}

		JSONObject result = new JSONObject();
		JSONArray array = new JSONArray();
		for (ValidationMessage c : context.getMessages()) {
			try {
				JSONObject code = new JSONObject();
				code.put("code", c.getMessage());
				array.put(code);
			} catch (Exception e) {
				LOGGER.error("Exception was thrown during JSON array forming" + e.getMessage());
			}
		}

		try {
			result.put("messages", array);
		} catch (Exception e) {
			LOGGER.error("Exception was thrown during JSON array forming" + e.getMessage());
		}
		return result;
	}

	@RequestMapping(value = "/averageChange", method = RequestMethod.POST)
	public @ResponseBody byte[] termChange(HttpServletRequest request, HttpServletResponse response,
			@RequestParam("rates") String rates) throws Exception {

		String[] splitted = rates.split(",");
		Double sum = 0.0;
		if (splitted[0] != "") {
			for (int i = 0; i < splitted.length; i++) {
				sum += Double.parseDouble(splitted[i]);
			}
			sum = sum / splitted.length;
		}

		return sum.toString().getBytes("UTF-8");
	}
}
