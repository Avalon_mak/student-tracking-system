package com.students.controller;

import com.students.data.discipline.domain.DisciplineDTO;
import com.students.data.discipline.model.Discipline;
import com.students.data.discipline.service.DisciplineService;
import com.students.data.rating.domain.RatingDTO;
import com.students.data.student.domain.StudentDTO;
import com.students.data.student.model.Student;
import com.students.utils.BreedcrumbsConfig;
import com.students.utils.ValidationContext;
import com.students.utils.ValidationMessage;
import com.students.utils.Validator;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class DisciplineController {

	private static final Logger LOGGER = LoggerFactory.getLogger(DisciplineController.class);

	private static final SimpleDateFormat FORMATTER = new SimpleDateFormat("dd/MM/yyyy");

	@Autowired
	private BreedcrumbsConfig breedcrumbs;

	@Autowired
	private DisciplineService disciplineService;

	@RequestMapping("/disciplineList")
	public ModelAndView discplineList(@RequestParam Map<String, String> allRequestParams, ModelMap model) {
		List<DisciplineDTO> disciplinesDTOList = new ArrayList<DisciplineDTO>();
		String currentPage = new String();
		List<Discipline> disciplines = new ArrayList<Discipline>();

		if (allRequestParams.size() != 0 && allRequestParams.get("searchInput") != "") {
			String filter = allRequestParams.get("filter");
			String[] forSearch = allRequestParams.get("searchInput").split(" ");
			disciplines = toSearch(forSearch, filter);
		} else {
			disciplines = disciplineService.listAll();
		}

		String page = allRequestParams.get("page");
		Map<String, Integer> pages = pagination(disciplines, page);

		for (int i = pages.get("start"); i < pages.get("end"); i++) {
			DisciplineDTO disciplineDTO = new DisciplineDTO();
			disciplineDTO.setId(String.valueOf(disciplines.get(i).getId()));
			disciplineDTO.setDisciplineName(disciplines.get(i).getDisciplineName());
			disciplinesDTOList.add(disciplineDTO);
		}

		return createMVForList("app.disciplineList", disciplinesDTOList, pages.get("currentPage"), pages.get("pagesCount"));
	}
	
	public List<Discipline> toSearch(String[] searchBody, String filter) {
		List<Discipline> disciplines = new ArrayList<Discipline>();
		if (filter.equals("name")) {
			String name = searchBody[0];
			Set<Discipline> res = new HashSet<Discipline>();
			res = this.disciplineService.getByName(name);
			for (Discipline d : res) {
				disciplines.add(d);
			}
		}
		return disciplines;
	}
	
	public Map<String, Integer> pagination(List<Discipline> disciplines, String page) {
		int start = 0;
		int end = 0;
		String currentPage = page;
		Map<String, Integer> result = new HashMap<String, Integer>();

		int pages = 0;
		if (disciplines.size() > 10) {
			pages = disciplines.size() / 10 + 1;
		} else {
			pages = 1;
		}

		if (disciplines.size() < 10 || page == null) {
			currentPage = "1";
		} else {
			Integer size = disciplines.size();
		}

		if (Integer.parseInt(currentPage) > 1 && Integer.parseInt(currentPage) <= pages) {
			end = 10 * Integer.parseInt(currentPage);
			start = end - 10;
			if (end > disciplines.size()) {
				end = disciplines.size();
			}
		} else {
			currentPage = "1";
			end = 10;
			if (end > disciplines.size()) {
				end = disciplines.size();
			}
		}

		result.put("pagesCount", pages);
		result.put("start", start);
		result.put("end", end);
		result.put("currentPage", Integer.parseInt(currentPage));

		return result;
	}
	
	public ModelAndView createMVForList(String viewName, List<DisciplineDTO> disciplines, Integer currentPage,
			Integer pagesCount) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String username = auth.getName();

		ModelAndView mv = new ModelAndView();
		mv.setViewName(viewName);
		mv.addObject("username", username);
		mv.addObject("disciplineList", disciplines);
		mv.addObject("currentPage", currentPage);
		mv.addObject("pages", pagesCount);

		return mv;
	}

	@RequestMapping(value = "/disciplineCreate", method = RequestMethod.GET)
	public ModelAndView disciplineCreateGet() {
		DisciplineDTO disciplineDTO = new DisciplineDTO();
		disciplineDTO.setDisciplineName("");

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String username = auth.getName();

		Map<String, List<String>> breedcrumbsUrls = this.breedcrumbs.getCodes("disciplineList");
		return ModelAndViewCreate("app.disciplineCreate" ,username, "discipline.create", disciplineDTO, "disciplineCreate", breedcrumbsUrls, null, null);
	}
	
	public ModelAndView ModelAndViewCreate(String viewName, String username, String title, DisciplineDTO disciplineDTO, String url, Map<String, List<String>> breedcrumbs,
			JSONObject validationResult, ValidationContext validationContext){
		ModelAndView mv = new ModelAndView();
		mv.setViewName(viewName);
		mv.addObject("username", username);
		mv.addObject("title", title);
		mv.addObject("discipline", disciplineDTO);
		mv.addObject("url", url);
		
		mv.addObject("breedcrumds", breedcrumbs.get("url"));
		mv.addObject("navNames", breedcrumbs.get("name"));
		
		mv.addObject("validateResult", validationResult);
		mv.addObject("messages", validationContext);

		return mv;
	}

	@RequestMapping(value = "/disciplineCreate", method = RequestMethod.POST)
	public ModelAndView disciplinereatePost(@ModelAttribute("discipline") DisciplineDTO disciplineDTO,
			BindingResult bindingResult) {

		Discipline discipline = new Discipline();
		discipline.setDisciplineName(disciplineDTO.getDisciplineName());

		List<ValidationMessage> messages;
		Validator validator = validationForPOSTs(disciplineDTO);

		JSONObject validationJSON = validationJSON(validator.getContext());

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String username = auth.getName();
		ModelAndView mv = new ModelAndView();
		if (validator.getContext().getMessages().size() > 0) {
			Map<String, List<String>> breedcrumbsUrls = this.breedcrumbs.getCodes("disciplineList");

			return ModelAndViewCreate("app.disciplineCreate", username, "discipline.create", disciplineDTO, "disciplineCreate", breedcrumbsUrls, validationJSON, validator.getContext());
		}
		mv.setViewName("redirect:disciplineList");

		disciplineService.save(discipline);

		return mv;
	}

	@RequestMapping(value = "/disciplineDelete", method = RequestMethod.POST)
	public String disciplineDeletePost(@RequestParam("list") String list) {

		String ids = "";

		String[] splitted = list.split(",");
		for (int i = 0; i < splitted.length; i++) {
			if (i < splitted.length - 1) {
				if (splitted[i].charAt(0) > 58 || splitted[i].charAt(0) < 47) {
					i++;
				} else {
					ids += splitted[i] + ",";
				}
			} else {
				ids += splitted[i];
			}
		}
		splitted = ids.split(",");

		List<Long> idsL = new ArrayList<Long>();

		for (int i = 0; i < splitted.length; i++) {
			idsL.add(Long.parseLong(splitted[i]));
		}

		disciplineService.delete(idsL);

		return "redirect:/app/disciplineList";
	}

	@RequestMapping(value = "/disciplineEdit", method = RequestMethod.GET)
	public ModelAndView disciplineEditGet(@RequestParam("id") Long id) {
		DisciplineDTO disciplineDTO = new DisciplineDTO();

		Discipline discipline = disciplineService.get(id);
		disciplineDTO.setId(discipline.getId().toString());
		disciplineDTO.setDisciplineName(discipline.getDisciplineName());

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String username = auth.getName();

		Map<String, List<String>> breedcrumbsUrls = this.breedcrumbs.getCodes("disciplineList");

		return ModelAndViewCreate("app.disciplineEdit", username, "discipline.edit", disciplineDTO, "disciplineEdit", breedcrumbsUrls, null, null);
	}

	@RequestMapping(value = "/disciplineEdit", method = RequestMethod.POST)
	public ModelAndView disciplineEditPost(@ModelAttribute("discipline") DisciplineDTO disciplineDTO,
			BindingResult bindingResult) {

		Discipline discipline = new Discipline();
		discipline.setId(Long.parseLong(disciplineDTO.getId()));
		discipline.setDisciplineName(disciplineDTO.getDisciplineName());

		Validator validator = validationForPOSTs(disciplineDTO);

		JSONObject validationJSON = validationJSON(validator.getContext());

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String username = auth.getName();

		ModelAndView mv = new ModelAndView();
		if (validator.getContext().getMessages().size() > 0) {
			Map<String, List<String>> urls = this.breedcrumbs.getCodes("disciplineList");
			return ModelAndViewCreate("app.disciplineCreate", username, "discipline.edit", disciplineDTO, "disciplineCreate", urls, validationJSON, validator.getContext());
		}
		
		mv.setViewName("redirect:app.disciplineList");

		disciplineService.save(discipline);

		return mv;
	}

	public JSONObject validationJSON(ValidationContext context) {

		if (context.getMessages().size() == 0) {
			return new JSONObject();
		}

		JSONObject result = new JSONObject();
		JSONArray array = new JSONArray();
		for (ValidationMessage c : context.getMessages()) {
			try {
				JSONObject code = new JSONObject();
				code.put("code", c.getMessage());
				array.put(code);
			} catch (Exception e) {
				LOGGER.error("Exception was thrown during JSON array forming" + e.getMessage());
			}
		}

		try {
			result.put("messages", array);
		} catch (Exception e) {
			LOGGER.error("Exception was thrown during JSON array forming" + e.getMessage());
		}
		return result;
	}
	
	public Validator validationForPOSTs(DisciplineDTO disciplineDTO){
		ValidationContext validationContext = new ValidationContext();
		Validator validator = new Validator();
		validator.setContext(validationContext);
		validator.validateDiscipline(disciplineDTO);
		
		return validator;
	}
}
