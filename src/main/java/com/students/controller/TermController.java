package com.students.controller;

import com.students.data.discipline.domain.DisciplineDTO;
import com.students.data.discipline.model.Discipline;
import com.students.data.discipline.service.DisciplineService;
import com.students.data.term.domain.TermDTO;
import com.students.data.term.model.Term;
import com.students.data.term.service.TermService;
import com.students.utils.BreedcrumbsConfig;
import com.students.utils.ValidationContext;
import com.students.utils.Validator;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * Created by Avalon on 28.08.2015.
 */


@Controller
public class TermController {

    @Autowired
    private TermService termService;

    @Autowired
    private DisciplineService disciplineService;

    private BreedcrumbsConfig breedcrumbs;

    @RequestMapping("/termList")
    public ModelAndView termList(@RequestParam Map<String, String> allRequestParams, ModelMap model) {
        List<TermDTO> termDTOlist = new ArrayList<TermDTO>();
        List<Term> terms = new ArrayList<Term>();
        String currentPage = new String();
    	
		if (allRequestParams.size() != 0 && allRequestParams.get("searchInput") != "") {
			String filter = allRequestParams.get("filter");
			String[] forSearch = allRequestParams.get("searchInput").split(" ");
			terms = toSearch(forSearch, filter);
		} else {
			terms = this.termService.listAll();
		}
		
		String page = allRequestParams.get("page");
		Map<String, Integer> pages = pagination(terms, page);
		
        for (int i = pages.get("start"); i < pages.get("end"); i++) {
            TermDTO termDTO = new TermDTO();
            termDTO.setId(String.valueOf(terms.get(i).getId()));
            termDTO.setTermName(terms.get(i).getTermName());

            termDTOlist.add(termDTO);
        }
        return createMVForList("app.termList", termDTOlist, pages.get("currentPage"), pages.get("pahesCount"));
    }
    
    public List<Term> toSearch(String[] searchBody, String filter) {
		List<Term> terms = new ArrayList<Term>();
		if (filter.equals("name")) {
			String name = searchBody[0];
			terms = this.termService.getByName(name);
		}
		return terms;
	}
	
	public Map<String, Integer> pagination(List<Term> disciplines, String page) {
		int start = 0;
		int end = 0;
		String currentPage = page;
		Map<String, Integer> result = new HashMap<String, Integer>();

		int pages = 0;
		if (disciplines.size() > 10) {
			pages = disciplines.size() / 10 + 1;
		} else {
			pages = 1;
		}

		if (disciplines.size() < 10 || page == null) {
			currentPage = "1";
		} else {
			Integer size = disciplines.size();
		}

		if (Integer.parseInt(currentPage) > 1 && Integer.parseInt(currentPage) <= pages) {
			end = 10 * Integer.parseInt(currentPage);
			start = end - 10;
			if (end > disciplines.size()) {
				end = disciplines.size();
			}
		} else {
			currentPage = "1";
			end = 10;
			if (end > disciplines.size()) {
				end = disciplines.size();
			}
		}

		result.put("pagesCount", pages);
		result.put("start", start);
		result.put("end", end);
		result.put("currentPage", Integer.parseInt(currentPage));

		return result;
	}
	
	public ModelAndView createMVForList(String viewName, List<TermDTO> disciplines, Integer currentPage,
			Integer pagesCount) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String username = auth.getName();

		ModelAndView mv = new ModelAndView();
		mv.setViewName(viewName);
		mv.addObject("username", username);
		mv.addObject("termsList", disciplines);
		mv.addObject("currentPage", currentPage);
		mv.addObject("pages", pagesCount);

		return mv;
	}

    @RequestMapping(value="/termCreate", method = RequestMethod.GET)
    public ModelAndView termCreateGet() {
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    	String username = auth.getName();
    	
        TermDTO termDTO = new TermDTO();

        termDTO.setTermName("");
        List<Discipline> disciplines = disciplineService.listAll();
        
        Map<String, List<String>> breedcrumbsUrls = this.breedcrumbs.getCodes("termList");

        return ModelAndViewCreate("app.termCreate", username, "term.create", termDTO, "rtermCreate", breedcrumbsUrls, null, null, disciplines, null);
    }
    
	public ModelAndView ModelAndViewCreate(String viewName, String username, String title, TermDTO termDTO, String url, Map<String, List<String>> breedcrumbs,
			JSONObject validationResult, ValidationContext validationContext, List<Discipline> disciplines, List<Discipline> selectedDisciplines){
		ModelAndView mv = new ModelAndView();
		mv.setViewName(viewName);
		mv.addObject("username", username);
		mv.addObject("title", title);
		mv.addObject("term", termDTO);
		mv.addObject("url", url);
		mv.addObject("disciplines", disciplines);
		mv.addObject("selectedDisciplines", selectedDisciplines);
		
		
		mv.addObject("breedcrumds", breedcrumbs.get("url"));
		mv.addObject("navNames", breedcrumbs.get("name"));
		
		mv.addObject("validateResult", validationResult);
		mv.addObject("messages", validationContext);

		return mv;
	}

    @RequestMapping(value="/termCreate", method = RequestMethod.POST)
    public String termCreatePost(HttpServletRequest request, @ModelAttribute("term") TermDTO termDTO) {
        List<String> disciplinesList = new ArrayList<String>();

        Map <Object, Object> requestParam = request.getParameterMap();

        String[] disciplinesFromPage = (String[]) requestParam.get("selectedDisciplines");

        String[] clearIds = disciplinesFromPage[0].split("[/s \",\"]");



        for(int i = 0; i < clearIds.length; i++){
            if(clearIds[i].length() != 0) {
                disciplinesList.add(clearIds[i]);
            }
        }

        Term term = new Term();
        Discipline tempVariableDiscipline;
        term.setTermName(termDTO.getTermName());
        Set<Discipline> selectedTermDisciplines = new HashSet<Discipline>();
        for(String discs : disciplinesList){
            Long id = Long.parseLong(discs);
            tempVariableDiscipline = disciplineService.get(id);
            selectedTermDisciplines.add(tempVariableDiscipline);
        }

        term.setDisciplines(selectedTermDisciplines);

        termService.save(term);

        return "redirect:/app/termList";
    }

    @RequestMapping("/termDetails")
    public ModelAndView termView(@RequestParam("id") Long id) {
        TermDTO termDTO = new TermDTO();

        Term term = termService.get(id);
        List<String> currentTermDisciplines = new ArrayList<String>();
        for(Discipline d : term.getDisciplines()){
            currentTermDisciplines.add(d.getDisciplineName());
        }

        termDTO.setId(String.valueOf(term.getId()));
        termDTO.setTermName(term.getTermName());
        termDTO.setDisciplines(currentTermDisciplines);
        
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    	String username = auth.getName();
    	

        ModelAndView mv = new ModelAndView();
        mv.setViewName("app.termDetails");
        mv.addObject("username", username);
        mv.addObject("term", termDTO);
        return mv;
    }

    @RequestMapping(value="/termDelete", method = RequestMethod.POST)
    public String termDeletePost(@RequestParam("list") String list) {

        String ids = "";

        String[] splitted = list.split(",");
        for(int i = 0; i < splitted.length; i++){
            if(i < splitted.length-1) {
                if(splitted[i].charAt(0) > 58 || splitted[i].charAt(0) < 47){
                    i++;
                } else {
                    ids += splitted[i] + ",";
                }
            } else {
                ids += splitted[i];
            }
        }
        splitted = ids.split(",");

        List<Long> idsL = new ArrayList<Long>();

        for(int i = 0 ; i < splitted.length; i++){
            idsL.add(Long.parseLong(splitted[i]));
        }

        termService.deleteTermsByIds(idsL);

        return "redirect:/app/termList";
    }

    @RequestMapping(value="/termEdit", method = RequestMethod.GET)
    public ModelAndView termEditGet(@RequestParam("id") Long id) {
        TermDTO termDTO = new TermDTO();

        Term term = termService.get(id);
        termDTO.setId(term.getId().toString());
        termDTO.setTermName(term.getTermName());

        Set<Discipline> disciplinesInTerm = term.getDisciplines();
        List<Discipline> unselectedDisciplines = disciplineService.listAll();

        Discipline [] toDelete = new Discipline[unselectedDisciplines.size()];
        int counter = 0;
        
        List<Discipline> selectedDiscioliens = new ArrayList<Discipline>();
        for(Discipline inTerm : disciplinesInTerm){
        	selectedDiscioliens.add(inTerm);
            for(Discipline all : unselectedDisciplines){
                if(all.getId() == inTerm.getId()){
                    toDelete[counter] = all;
                    counter++;
                }
            }
        }

        for(int i = 0 ; i < counter; i++){
            unselectedDisciplines.remove(toDelete[i]);
        }
        
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    	String username = auth.getName();
    	
        Map<String, List<String>> breedcrumbsUrls = this.breedcrumbs.getCodes("termList");


        ModelAndView mv = new ModelAndView();
        mv.setViewName("app.termEdit");
        mv.addObject("username", username);
        mv.addObject("term", termDTO);
        mv.addObject("disciplines", unselectedDisciplines);
        mv.addObject("selectedDisciplines", disciplinesInTerm);
        mv.addObject("title", "term.edit");
        mv.addObject("url", "termEdit");

        return ModelAndViewCreate("app.termEdit", username, "term.edit", termDTO, "termEdit", breedcrumbsUrls, null, null, unselectedDisciplines, selectedDiscioliens);
    }

    @RequestMapping(value="/termEdit", method = RequestMethod.POST)
    public String termEditPost(HttpServletRequest request, @ModelAttribute("term")  TermDTO termDTO, BindingResult bindingResult) {
        List<String> idsFromPage = new ArrayList<String>();
        Map <Object, Object> requestParam = request.getParameterMap();
        String[] disciplines = (String[]) requestParam.get("selectedDisciplines");
        String[] clearIds = disciplines[0].split("[/s \",\"]");

        for(int i = 0; i < clearIds.length; i++){
            if(clearIds[i].length() != 0) {
                idsFromPage.add(clearIds[i]);
            }
        }

        Term old = termService.get(Long.parseLong(termDTO.getId()));
        Set<Discipline> oldList = old.getDisciplines();
        String [] temp = new String[oldList.size()];
        int counter = 0;
        for(Discipline oldDiscipline : oldList){
            for(String newDiscipline : idsFromPage){
                if(oldDiscipline.getId().toString() == newDiscipline){
                    temp[counter] = newDiscipline;
                }
            }
        }
        for(int i = 0; i < counter; i++){
            idsFromPage.remove(temp[i]);
        }

        Iterator <Discipline> iterator = oldList.iterator();
        int i = 0;

        while (iterator.hasNext()){
            if(iterator.next().getId().toString() == temp[i]){
                oldList.remove(iterator.next());
            }
        }

        List<Long> oldsIds = new ArrayList<Long>();
        iterator = oldList.iterator();
        while(iterator.hasNext()){
            oldsIds.add(iterator.next().getId());
        }

        if(oldList.size() > 0){
            termService.deleteDisciplineFromTerm(oldsIds);
        }
        Set<Discipline> newDisciplines = new HashSet<Discipline>();
        if(idsFromPage.size() > 0){
            for(String ids : idsFromPage) {
                 newDisciplines.add(disciplineService.get(Long.parseLong(ids)));
            }
        }

        Term term = new Term();
        term.setId(old.getId());
        term.setTermName(termDTO.getTermName());
        term.setDisciplines(newDisciplines);

        termService.save(term);

        return "redirect:/app/termList";
    }
}
