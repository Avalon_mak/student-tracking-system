package com.students.controller;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class Home {

    @RequestMapping("/home")
    public ModelAndView home() {
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    	String username = auth.getName();
    	
    	ModelAndView mv = new ModelAndView();
    	mv.setViewName("app.homepage");	
    	mv.addObject("username", username);
        return mv;
    }
}