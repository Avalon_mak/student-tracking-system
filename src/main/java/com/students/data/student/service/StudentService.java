package com.students.data.student.service;

import java.util.List;

import com.students.data.student.model.Student;


public interface StudentService {

    Student get(Long id);

    void save(Student student);
    
    List<Student> searchByName(String firstName, String secondName);
    
    List<Student> searchByGroup(String group);
    
    List<Student> searchByDate(String date);
    
    void delete(String ids);

    List<Student> listAll();
}
