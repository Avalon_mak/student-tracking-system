package com.students.data.student.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.students.data.student.dao.StudentDAO;
import com.students.data.student.model.Student;

@Service
public class StudentServiceImpl implements StudentService {

    @Autowired
    private StudentDAO dao;

    @Override
    @Transactional(readOnly = true)
    public Student get(Long id) {
        return dao.get(id);
    }

    @Override
    @Transactional
    public void save(Student student) {
        this.dao.save(student);
    }
    @Override
    @Transactional
    public void delete(String ids) {
    	this.dao.delete(ids);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Student> listAll() {
        return dao.listAll();
    }

	@Override
    @Transactional(readOnly = true)
	public List<Student> searchByName(String firstName, String secondName) {
		return dao.searchByName(firstName, secondName);
	}

	@Override
    @Transactional(readOnly = true)
	public List<Student> searchByGroup(String group) {
		return dao.searchByGroup(group);
	}

	@Override
    @Transactional(readOnly = true)
	public List<Student> searchByDate(String date) {
		return dao.searchByDate(date);
	}

}
