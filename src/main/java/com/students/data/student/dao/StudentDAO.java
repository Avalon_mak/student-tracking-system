package com.students.data.student.dao;

import java.util.List;

import com.students.data.student.model.Student;


public interface StudentDAO {

    void save(Student student);

    void delete(String ids);

    List<Student> listAll();

    List<Student> searchByName(String firstName, String secondName);
    
    List<Student> searchByGroup(String group);
    
    List<Student> searchByDate(String date);

    Student get(Long id);

}
