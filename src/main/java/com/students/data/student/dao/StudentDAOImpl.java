package com.students.data.student.dao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.students.data.student.model.Student;

@Repository
public class StudentDAOImpl implements StudentDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public void save(Student student) {
        this.getSession().saveOrUpdate(student);
    }

    public void delete(String ids) {
    	String query = "delete from Student where id in (" + ids + ")";
    	this.getSession().createSQLQuery(query).executeUpdate();
    }

    @Override
    public List<Student> listAll() {
        Criteria c = this.getSession().createCriteria(Student.class);
        return (List<Student>) c.list();
    }

    @Override
    public Student get(Long id) {
        return (Student) this.getSession().get(Student.class, id);
    }

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

	@Override
	public List<Student> searchByName(String firstName, String lastName) {
		List<Student> res = this.getSession().createCriteria(Student.class).add(Restrictions.eq("name", firstName)).add(Restrictions.eq("lastName", lastName)).list();
		return res;
	}

	@Override
	public List<Student> searchByGroup(String group) {
		List<Student> res = this.getSession().createCriteria(Student.class).add(Restrictions.eq("universityGroup", group)).list();
		return res;
	}

	@Override
	public List<Student> searchByDate(String date) {
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		Date d = new Date();
		try {
			d = formatter.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		List<Student> res = this.getSession().createCriteria(Student.class).add(Restrictions.eq("admissionDate", d)).list();
		return res;
	}

}

