package com.students.data.term.domain;

import org.springframework.stereotype.Repository;

import java.util.List;

public class TermDTO {
    private String id;

    public String getTermName() {
        return termName;
    }

    public void setTermName(String termName) {
        this.termName = termName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    private String termName;

    private List<String> disciplines;

    public List<String> getDisciplines() {
        return disciplines;
    }

    public void setDisciplines(List<String> disciplines) {
        this.disciplines = disciplines;
    }
}
