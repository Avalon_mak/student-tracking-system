package com.students.data.term.dao;

import com.students.data.discipline.model.Discipline;
import com.students.data.term.model.Term;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Repository
public class TermDAOImpl implements TermDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(TermDAOImpl.class);

    @Autowired
    private SessionFactory sessionFactory;

    public void save(Term term) {
        this.getSession().saveOrUpdate(term);
    }

    @Override
    public void deleteDisciplineFromTerm(List<Long> ids) {
        LOGGER.info("[Discipline]s with ids {} are deleted.", ids);

        String idsForQuery = "";
        int counter = 0;
        for(Long id : ids){
            counter++;
            if(counter < ids.size()) {
                idsForQuery += id.toString() + ", ";
            } else {
                idsForQuery += id.toString();
            }
        }

        String firstQuery = "delete from discipline_term where discipline_id in (" + idsForQuery + ")";
        this.getSession().createSQLQuery(firstQuery).executeUpdate();

        LOGGER.debug("[Discipline]s of [Term]s with ids {} have been deleted successfully.", ids);
    }

    public void deleteTermsByIds(List<Long> ids) {
        LOGGER.info("[Term]s with ids {} are deleted.", ids);

        String idsForQuery = "";
        int counter = 0;
        for(Long id : ids){
            counter++;
            if(counter < ids.size()) {
                idsForQuery += id.toString() + ", ";
            } else {
                idsForQuery += id.toString();
            }
        }

        String firstQuery = "delete from discipline_term where discipline_id in (" + idsForQuery + ")";
        this.getSession().createSQLQuery(firstQuery).executeUpdate();

        LOGGER.debug("[Discipline]s of [Term]s with ids {} have been deleted successfully.", ids);

        String secondQuery = "delete from Term where id in (" + idsForQuery + ")";
        this.getSession().createSQLQuery(secondQuery).executeUpdate();

        LOGGER.info("[Term]s with ids {} have been deleted successfully.", ids);
    }

    @Override
    public List<Term> listAll() {
        Criteria c =  this.getSession().createCriteria(Term.class);
        return c.list();
    }

    @Override
    public Term get(Long id) {
        return (Term) this.getSession().get(Term.class, id);
    }

	@Override
	public List<Term> getByName(String name) {
        List<Term> res = this.getSession().createCriteria(Term.class).add(Restrictions.eq("termName", name)).list();
        return res;
	}
	
	private Session getSession() {
		return sessionFactory.getCurrentSession();
	}
}
