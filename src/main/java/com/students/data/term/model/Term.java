package com.students.data.term.model;

import com.students.data.discipline.model.Discipline;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
public class Term {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String termName;

    @ManyToMany
    @JoinTable(name="discipline_term", joinColumns=@JoinColumn(name="term_id"), inverseJoinColumns=@JoinColumn(name="discipline_id"))
    private Set<Discipline> disciplines;



    public Set<Discipline> getDisciplines() {
        return disciplines;
    }

    public void setDisciplines(Set<Discipline> disciplines) {
        this.disciplines = disciplines;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTermName() {
        return termName;
    }

    public void setTermName(String termName) {
        this.termName = termName;
    }
}
