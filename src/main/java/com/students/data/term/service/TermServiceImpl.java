package com.students.data.term.service;

import com.students.data.discipline.model.Discipline;
import com.students.data.student.dao.StudentDAO;
import com.students.data.term.dao.TermDAO;
import com.students.data.term.model.Term;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

@Service
public class TermServiceImpl implements TermService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TermServiceImpl.class);

    @Autowired
    private TermDAO dao;

    @Override
    @Transactional(readOnly = true)
    public Term get(Long id) {
        Term term = dao.get(id);
        term.getDisciplines().size();
        return term;
    }

    @Override
    @Transactional
    public void deleteDisciplineFromTerm(List<Long> ids) {
        this.dao.deleteDisciplineFromTerm(ids);
    }

    @Override
    @Transactional
    public void save(Term term) {
        this.dao.save(term);
    }

    @Override
    @Transactional
    public void deleteTermsByIds(List<Long> ids) {
        LOGGER.info("Get ids {}", ids);

        this.dao.deleteTermsByIds(ids);

        LOGGER.info("Ids {} sent to DAO", ids);
    }


    @Override
    @Transactional(readOnly = true)
    public List<Term> listAll() {
        List<Term> terms = dao.listAll();
        for(Term term: terms){
            term.getDisciplines().size();
        }
        return terms;
    }

	@Override
	public List<Term> getByName(String name) {
		return this.dao.getByName(name);
	}
}
