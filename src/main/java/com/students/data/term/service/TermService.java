package com.students.data.term.service;

import com.students.data.discipline.model.Discipline;
import com.students.data.term.model.Term;

import java.util.List;
import java.util.Set;

/**
 * Created by Avalon on 28.08.2015.
 */
public interface TermService {

    Term get(Long id);

    void save(Term term);

    void deleteTermsByIds(List<Long> ids);

    void deleteDisciplineFromTerm(List<Long> ids);

    List<Term> listAll();

    List<Term> getByName(String name);
}
