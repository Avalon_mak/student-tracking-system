package com.students.data.discipline.model;

import com.students.data.term.model.Term;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Discipline {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String disciplineName;

    @ManyToMany
    @JoinTable(name="discipline_term", joinColumns=@JoinColumn(name="discipline_id"), inverseJoinColumns=@JoinColumn(name="term_id"))
    private Set<Term> terms;

    public Set<Term> getTerms() {
        return terms;
    }

    public void setTerms(Set<Term> terms) {
        this.terms = terms;
    }



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDisciplineName() {
        return disciplineName;
    }

    public void setDisciplineName(String disciplineName) {
        this.disciplineName = disciplineName;
    }

}