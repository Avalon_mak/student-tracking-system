package com.students.data.discipline.dao;

import com.students.data.discipline.model.Discipline;

import java.util.List;
import java.util.Set;

/**
 * Created by Avalon on 18.08.2015.
 */
public interface DisciplineDAO {

    void save(Discipline discipline);

    void delete(List<Long> ids);

    List<Discipline> listAll();

    Discipline get(Long id);

    Set<Discipline> getByName(String name);
}
