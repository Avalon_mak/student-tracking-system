package com.students.data.discipline.dao;

import com.students.data.discipline.model.Discipline;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaQuery;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

@Repository
public class DisciplineDAOImpl implements DisciplineDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public void save(Discipline discipline) {
        this.getSession().saveOrUpdate(discipline);
    }

    public void delete(List<Long> ids) {
    	
        String idsForQuery = "";
        int counter = 0;
        for(Long id : ids){
            counter++;
            if(counter < ids.size()) {
                idsForQuery += id.toString() + ", ";
            } else {
                idsForQuery += id.toString();
            }
        }

        String firstQuery = "delete from discipline_term where discipline_id in (" + idsForQuery + ")";
        this.getSession().createSQLQuery(firstQuery).executeUpdate();
        String secondQery = "delete from rating where discipline_id in (" + idsForQuery + ")";
        this.getSession().createSQLQuery(secondQery).executeUpdate();
        String lastQuery = "delete from discipline where id in (" + idsForQuery + ")";
        this.getSession().createSQLQuery(lastQuery).executeUpdate();
    }

    @Override
    public List<Discipline> listAll() {
        Criteria c =  this.getSession().createCriteria(Discipline.class);
        return c.list();
    }

    @Override
    public Discipline get(Long id) {
        return (Discipline) this.getSession().get(Discipline.class, id);
    }

    @Override
    public Set<Discipline> getByName(String name) {
        List<Discipline> res = this.getSession().createCriteria(Discipline.class).add(Restrictions.eq("disciplineName", name)).list();
        Set<Discipline> result = new HashSet<Discipline>();
        if(res.size() > 1){
            result.addAll(res);
        } else if (res.size() == 1) {
            Discipline test = res.get(0);
            result.add(test);
        }
        return result;
    }

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }
}
