package com.students.data.discipline.service;

import com.students.data.discipline.model.Discipline;

import java.util.List;
import java.util.Set;

public interface DisciplineService {

    Discipline get(Long id);

    void save(Discipline student);

    void delete(List<Long> ids);

    List<Discipline> listAll();

    Set<Discipline> getByName(String name);

}
