package com.students.data.discipline.service;

import com.students.data.discipline.model.Discipline;
import com.students.data.discipline.dao.DisciplineDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

@Service
public class DisciplineServiceImpl implements DisciplineService {

    @Autowired
    private DisciplineDAO dao;

    @Override
    @Transactional(readOnly = true)
    public Discipline get(Long id) {
        return dao.get(id);
    }

    @Override
    @Transactional
    public void save(Discipline discipline) {
        dao.save(discipline);
    }

    @Override
    @Transactional
    public Set<Discipline> getByName(String name) {
        return dao.getByName(name);
    }

    @Override
    @Transactional
    public void delete(List<Long> ids) {
        dao.delete(ids);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Discipline> listAll() {
        return dao.listAll();
    }
}
