package com.students.data.discipline.domain;

/**
 * Created by Avalon on 18.08.2015.
 */
public class DisciplineDTO {
    private String id;
    private String disciplineName;

    public String getDisciplineName() {
        return disciplineName;
    }

    public void setDisciplineName(String disciplineName) {
        this.disciplineName = disciplineName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
