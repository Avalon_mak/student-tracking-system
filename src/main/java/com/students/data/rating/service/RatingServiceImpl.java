package com.students.data.rating.service;

import com.students.data.rating.dao.RatingDAO;
import com.students.data.rating.model.Rating;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Avalon on 09.10.2015.
 */
@Service
public class RatingServiceImpl implements RatingService {
    @Autowired
    private RatingDAO dao;

    @Override
    @Transactional(readOnly = true)
    public Rating get(Long id) {
        return this.dao.get(id);
    }

    @Override
    @Transactional
    public void save(Rating rating) {
        this.dao.save(rating);
    }

    @Override
    @Transactional
    public List<Rating> getByTermAndStudId(Long studId, Long termId) {
        return this.dao.getByTermAndStudId(studId, termId);
    }

    @Override
    @Transactional
    public void delete(String studentId) {
        this.dao.delete(studentId);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Rating> listAll() {
        return null;
    }
}
