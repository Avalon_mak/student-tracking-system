package com.students.data.rating.service;

import com.students.data.rating.model.Rating;

import java.util.List;

/**
 * Created by Avalon on 09.10.2015.
 */
public interface RatingService {
    Rating get(Long id);

    void save(Rating rating);

    void delete(String studentId);

    List<Rating> listAll();

    List<Rating> getByTermAndStudId(Long studId, Long termId);
}
