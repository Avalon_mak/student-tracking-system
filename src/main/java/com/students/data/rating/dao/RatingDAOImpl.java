package com.students.data.rating.dao;

import com.students.data.rating.model.Rating;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Avalon on 09.10.2015.
 */
@Repository
public class RatingDAOImpl implements RatingDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public void save(Rating rating) {
        this.getSession().saveOrUpdate(rating);
    }

    @Override
    public List<Rating> getByTermAndStudId(Long studId, Long termId) {
        Criteria c = this.getSession().createCriteria(Rating.class);
        c.add(Restrictions.eq("student.id", studId));
        c.add(Restrictions.eq("term.id", termId));

        return c.list();
    }

    @Override
    public void delete(String ids) {
        String query = "delete from Rating where student_id in (" + ids + ")";
        this.getSession().createSQLQuery(query).executeUpdate();
    }

    @Override
    public Rating get(Long id) {
        return (Rating) this.getSession().get(Rating.class, id);
    }

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }
}
