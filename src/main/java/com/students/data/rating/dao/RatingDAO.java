package com.students.data.rating.dao;

import com.students.data.rating.model.Rating;

import java.util.List;

/**
 * Created by Avalon on 09.10.2015.
 */
public interface RatingDAO {

    void save(Rating rating);

    void delete(String ids);

    Rating get(Long id);

    List<Rating> getByTermAndStudId(Long studId, Long termId);

}
