package com.students.data.rating.domain;

import com.students.data.discipline.model.Discipline;
import com.students.data.term.model.Term;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.List;
import java.util.Set;

/**
 * Created by Avalon on 21.09.2015.
 */
public class RatingDTO {
    private String[] id;

    private List<String> disciplinesIds;

    private List<String> disciplinesNames;

    private String term;

    private List<String> rating;

    public List<String> getDisciplinesNames() {
        return disciplinesNames;
    }

    public void setDisciplinesNames(List<String> disciplinesNames) {
        this.disciplinesNames = disciplinesNames;
    }

    public String[] getId() {
        return id;
    }

    public void setId(String[] id) {
        this.id = id;
    }

    public List<String> getDisciplinesIds() {
        return disciplinesIds;
    }

    public void setDisciplinesIds(List<String> disciplinesIds) {
        this.disciplinesIds = disciplinesIds;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public List<String> getRating() {
        return rating;
    }

    public void setRating(List<String> rating) {
        this.rating = rating;
    }
}
