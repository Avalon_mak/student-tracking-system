package com.students.utils;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.util.List;

/**
 * Created by Avalon on 14.10.2015.
 */
public class CustomTag extends SimpleTagSupport {

    private ValidationContext code;
    private String field;

    public CustomTag(){

    }

    public void setCode(ValidationContext code) {
        this.code = code;
    }

    public void setField(String field) {
        this.field = field;
    }

    @Override
    public void doTag() throws JspException, IOException {
        JspWriter out = getJspContext().getOut();

        if(this.code == null){
            out.println("");
        } else {
            for (ValidationMessage vm : code.getMessages()) {
                if (vm.getMessage() == field) {
                    out.println("Field are empty.");
                }
            }
        }
    }

}
