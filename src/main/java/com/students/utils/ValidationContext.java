package com.students.utils;

import java.util.ArrayList;
import java.util.List;


public class ValidationContext {
    private List<ValidationMessage> messages = new ArrayList<ValidationMessage>();
    private String code;

    public void getForField(String fieldName){
        ValidationMessage message = new ValidationMessage();
        message.setMessage(fieldName);
        this.messages.add(message);
    }

    public void getForRating(String field) {
        ValidationMessage message = new ValidationMessage();
        message.setMessage(field);
        this.messages.add(message);
    }

    public List<ValidationMessage> getMessages() {
        return messages;
    }
}
