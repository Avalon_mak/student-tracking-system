package com.students.utils;

/**
 * Created by Avalon on 14.10.2015.
 */
public enum Severity {
        INFO,
        WARNING,
        ERROR;
}
