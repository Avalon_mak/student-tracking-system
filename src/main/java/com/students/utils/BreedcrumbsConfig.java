package com.students.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

@Component
public class BreedcrumbsConfig {
	@Resource(name = "breedcrumbs")
	private Properties property;
	
	private static String URL = ".url";
	private static String CAPTION = ".caption";

	public Map<String, List<String>> getCodes(String key) {
		Map<String, List<String>> result = new HashMap<String, List<String>>();
		String[] splitted = key.split("\\.");
		String temp = splitted[0];
		
		List<String> names = new ArrayList<String>();
		List<String> urls = new ArrayList<String>();
		
		if(splitted.length > 1) {
			urls.add(this.property.getProperty(temp + this.URL)); 
			names.add(this.property.getProperty(temp + this.CAPTION)); 
			for(int i = 1; i < splitted.length; i++){
				temp += "." + splitted[i];
				urls.add(this.property.getProperty(temp + this.URL)); 
				names.add(this.property.getProperty(temp + this.CAPTION)); 
			}
		} else {
			urls.add(this.property.getProperty(temp + this.URL)); 
			names.add(this.property.getProperty(temp + this.CAPTION)); 
		}
		
		result.put("name", names);
		result.put("url", urls);

		return result;
	}
}
