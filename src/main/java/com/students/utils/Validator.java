package com.students.utils;

import com.students.data.discipline.domain.DisciplineDTO;
import com.students.data.rating.domain.RatingDTO;
import com.students.data.student.domain.StudentDTO;
import com.students.data.term.domain.TermDTO;

import java.util.List;

public class Validator {
	private ValidationContext context;

	public List<ValidationMessage> validateStudent(StudentDTO studentDTO, RatingDTO ratingDTO) {

		if (studentDTO.getName() == "") {
			context.getForField("name");
		}
		if (studentDTO.getUniversityGroup() == "") {
			context.getForField("universityGroup");
		}
		if (studentDTO.getLastName() == "") {
			context.getForField("lastName");
		}
		if (studentDTO.getAdmissionDate() == "") {
			context.getForField("admissionDate");
		}
		if (ratingDTO.getTerm() != null) {
			for (int i = 0; i < ratingDTO.getRating().size(); i++) {
				if (ratingDTO.getRating().get(i) == "") {
					context.getForField("rating" + i);
				} else {
					for (int j = 0; j < ratingDTO.getRating().get(i).length(); j++) {
						if (ratingDTO.getRating().get(i).charAt(j) < 49
								&& ratingDTO.getRating().get(i).charAt(j) > 57) {
							context.getForRating("rating" + i);
						}
					}
				}
			}
		} else {
			context.getForField("currentTerm");
		}
		return context.getMessages();
	}

	public List<ValidationMessage> validateTerm(TermDTO termDTO) {
		if (termDTO.getTermName() == "") {
			context.getForField("termName");
		}
		if (termDTO.getDisciplines().size() == 0) {
			context.getForField("disciplines");
		}

		return context.getMessages();
	}

	public List<ValidationMessage> validateDiscipline(DisciplineDTO disciplineDTO) {
		if (disciplineDTO.getDisciplineName() == "") {
			context.getForField("disciplineName");
		}
		return context.getMessages();
	}

	public ValidationContext getContext() {
		return context;
	}

	public void setContext(ValidationContext context) {
		this.context = context;
	}
}
