package com.students.utils;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class BreedcrumdsWithId {
	
	public List<String> breedcrumb(Map<String, List<String>> urls, String id){
		List<String> withId = urls.get("url");
		Iterator<String> i = withId.iterator();
		while (i.hasNext()) {
			String link = i.next();
			if (link.equals("/app/studentProgress?id=${id}")) {
				link.substring(link.length() - 4, link.length());
				link += id;
				i.remove();
				withId.add(link);
				break;
			}
		}
		
		return withId;
	}

}
