package com.students.data.student.dao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.students.data.student.dao.StudentDAO;
import com.students.data.student.model.Student;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:application-context.xml" })
@EnableTransactionManagement
public class StudentDAOImplTest {

	@Autowired
	private StudentDAO dao;

    @Autowired
    private SessionFactory sessionFactory;
	
	@Test
	@Transactional
	public void save() throws Exception {
		
		// GIVEN
		Student expected = new Student();
		SimpleDateFormat FORMATTER = new SimpleDateFormat("dd/MM/yyyy");
		Date admissionDate = FORMATTER.parse("11/11/0011");

		expected.setAdmissionDate(admissionDate);
		expected.setLastName("seventhTest");
		expected.setName("seventhTest2");
		expected.setUniversityGroup("seventhTestGroup");
		
		
		// save new object
		this.dao.save(expected);
		
		// WHEN
		int size = this.dao.listAll().size()-1;
		long id = this.dao.listAll().get(size).getId();
		
		//THEN
		Student actual = this.dao.get(id);

		Assert.assertEquals(expected.getId(), actual.getId());
		Assert.assertEquals(expected.getName(), actual.getName());
		Assert.assertEquals(expected.getLastName(), actual.getLastName());
		Assert.assertEquals(expected.getUniversityGroup(), actual.getUniversityGroup());
		Assert.assertEquals(expected.getAdmissionDate().getTime(), actual.getAdmissionDate().getTime());
	}
	
	@Test
	@Transactional
	public void update() throws Exception {
		
		// GIVEN
		Student actual = new Student();
		SimpleDateFormat FORMATTER = new SimpleDateFormat("dd/MM/yyyy");
		Date admissionDate = FORMATTER.parse("11/11/0011");

		actual.setAdmissionDate(admissionDate);
		actual.setLastName("eighthTest");
		actual.setName("eighthTest2");
		actual.setUniversityGroup("seventhTestGroup");

		// save new object
		this.dao.save(actual);
		this.sessionFactory.getCurrentSession().flush();
		this.sessionFactory.getCurrentSession().clear();
		
		// WHEN (update last name)
		actual = this.dao.get(actual.getId());
		
		String newName = "eighthTestEdited";
		actual.setLastName(newName);
		
		this.dao.save(actual);
		this.sessionFactory.getCurrentSession().flush();
		this.sessionFactory.getCurrentSession().clear();

		// THEN
		actual = this.dao.get(actual.getId());
		
		Assert.assertEquals(newName, actual.getLastName());
	}

	@Test
	@Transactional
	public void get() throws Exception {

		// GIVEN
		Student expected = new Student();

		SimpleDateFormat FORMATTER = new SimpleDateFormat("dd/MM/yyyy");
		Date admissionDate = FORMATTER.parse("11/11/0011");

		expected.setAdmissionDate(admissionDate);
		expected.setLastName("sixthsTest");
		expected.setName("sixthsTest2");
		expected.setUniversityGroup("sixthsTestGroup");

		this.dao.save(expected);
		
		// WHEN
		int size = this.dao.listAll().size()-1;
		long id = this.dao.listAll().get(size).getId();
		
		// THEN
		Student actual = this.dao.get(id);
		expected.setId(id);

		Assert.assertEquals(expected.getId(), actual.getId());
		Assert.assertEquals(expected.getLastName(), actual.getLastName());
		Assert.assertEquals(expected.getName(), actual.getName());
		Assert.assertEquals(expected.getAdmissionDate().getTime(), actual.getAdmissionDate().getTime());
		Assert.assertEquals(expected.getUniversityGroup(), actual.getUniversityGroup());
	}

	@Test
	@Transactional
	public void delete() throws Exception {
		Student toDb = new Student();
		SimpleDateFormat FORMATTER = new SimpleDateFormat("dd/MM/yyyy");
		Date admissionDate = FORMATTER.parse("11/11/0011");

		toDb.setAdmissionDate(admissionDate);
		toDb.setLastName("fifthTest");
		toDb.setName("fifthTest2");
		toDb.setUniversityGroup("fifthTestGroup");

		this.dao.save(toDb);
		
		List<Student> all = this.dao.listAll();

		long id = all.get(all.size()-1).getId();
		toDb = this.dao.get(id);

		String ids = String.valueOf(id);
		this.dao.delete(ids);
		this.sessionFactory.getCurrentSession().clear();
		

		Student actual = this.dao.get(id);

		Assert.assertEquals(null, actual);
	}
	
	@Test
	@Transactional
	public void listAll() throws Exception {
		List<Student> all = this.dao.listAll();
		int actualSize = all.size();
		
		Student actual = new Student();
		SimpleDateFormat FORMATTER = new SimpleDateFormat("dd/MM/yyyy");
		Date admissionDate = FORMATTER.parse("11/11/0011");

		actual.setAdmissionDate(admissionDate);
		actual.setLastName("fourthTest");
		actual.setName("fourthTest2");
		actual.setUniversityGroup("fourthTestGroup");

		this.dao.save(actual);
		
		all = this.dao.listAll();
		
		Assert.assertEquals(all.size(), actualSize+1);
	}
	
	@Test
	@Transactional
	public void searchByName() throws Exception{
		Student expected = new Student();
		SimpleDateFormat FORMATTER = new SimpleDateFormat("dd/MM/yyyy");
		Date admissionDate = FORMATTER.parse("11/11/0011");

		expected.setAdmissionDate(admissionDate);
		expected.setLastName("thirdTest");
		expected.setName("thirdTest2");
		expected.setUniversityGroup("thirdTestGroup");

		this.dao.save(expected);
		
		List<Student> actual = this.dao.searchByName("thirdTest2", "thirdTest");
		
		Assert.assertEquals(expected.getName(), actual.get(0).getName());
		Assert.assertEquals(expected.getLastName(), actual.get(0).getLastName());
	}
	
	@Test
	@Transactional
	public void searchByGroup() throws Exception{
		Student expected = new Student();
		SimpleDateFormat FORMATTER = new SimpleDateFormat("dd/MM/yyyy");
		Date admissionDate = FORMATTER.parse("11/11/0011");

		expected.setAdmissionDate(admissionDate);
		expected.setLastName("secondTest");
		expected.setName("secondTest2");
		expected.setUniversityGroup("secondTestGroup");

		this.dao.save(expected);
		
		List<Student> actual = this.dao.searchByGroup("secondTestGroup");
		
		Assert.assertEquals(expected.getUniversityGroup(), actual.get(0).getUniversityGroup());
	}

	@Test
	@Transactional
	public void searchDate() throws Exception {
		Student expected = new Student();
		SimpleDateFormat FORMATTER = new SimpleDateFormat("dd/MM/yyyy");
		Date admissionDate = FORMATTER.parse("11/11/2011");
		
		expected.setAdmissionDate(admissionDate);
		expected.setLastName("firstTest");
		expected.setName("firstTest2");
		expected.setUniversityGroup("firstTestGroup");
		
		this.dao.save(expected);
		
		List<Student> actual = this.dao.searchByDate("11/11/2011");
		
		Assert.assertEquals(expected.getAdmissionDate().getTime(), actual.get(0).getAdmissionDate().getTime());
	}
}
