package com.students.data.rating.dao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.students.data.discipline.dao.DisciplineDAO;
import com.students.data.discipline.model.Discipline;
import com.students.data.rating.model.Rating;
import com.students.data.student.dao.StudentDAO;
import com.students.data.student.model.Student;
import com.students.data.term.dao.TermDAO;
import com.students.data.term.model.Term;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:application-context.xml" })
@EnableTransactionManagement
public class RationgDAOImplTest {
	
	@Autowired
	private RatingDAO ratingDAO;

	@Autowired
	private StudentDAO studentDAO;

	@Autowired
	private DisciplineDAO disciplineDAO;

	@Autowired
	private TermDAO termDAO;
	
	@Autowired
	private SessionFactory sessinFactory;
	
	@Test
	@Transactional
	public void save() throws Exception {
		// GIVEN
		Rating expected = new Rating();
		Discipline discipline = new Discipline();
		Term term = new Term();
		Student student = new Student();
		
		SimpleDateFormat FORMATTER = new SimpleDateFormat("dd/MM/yyyy");
		Date admissionDate = FORMATTER.parse("11/11/2011");
		
		student.setAdmissionDate(admissionDate);
		student.setLastName("firstTest");
		student.setName("firstTest2");
		student.setUniversityGroup("firstTestGroup");
		this.studentDAO.save(student);
		expected.setStudent(student);
		
		discipline.setDisciplineName("firstTestDiscipline");
		this.disciplineDAO.save(discipline);
		expected.setDiscipline(discipline);
		
		Set<Discipline> disciplines = new HashSet<Discipline>();
		disciplines.add(discipline);
		term.setTermName("firsttestTerm");
		term.setDisciplines(disciplines);
		this.termDAO.save(term);
		expected.setTerm(term);
		
		expected.setRating(50);
		
		// WHEN
		this.ratingDAO.save(expected);
		
		// THEN
		Rating actual = this.ratingDAO.get(expected.getId());
		
		Assert.assertEquals(expected.getDiscipline(), actual.getDiscipline());
		Assert.assertEquals(expected.getRating(), actual.getRating());
		Assert.assertEquals(expected.getStudent(), actual.getStudent());
	}
	
	@Test
	@Transactional
	public void update() throws Exception {
		// GIVEN
		Rating expected = new Rating();
		Discipline discipline = new Discipline();
		Term term = new Term();
		Student student = new Student();
		
		SimpleDateFormat FORMATTER = new SimpleDateFormat("dd/MM/yyyy");
		Date admissionDate = FORMATTER.parse("11/11/2011");
		
		student.setAdmissionDate(admissionDate);
		student.setLastName("secondTest");
		student.setName("secondTest2");
		student.setUniversityGroup("secondTestGroup");
		this.studentDAO.save(student);
		expected.setStudent(student);
		
		discipline.setDisciplineName("secondTestDiscipline");
		this.disciplineDAO.save(discipline);
		expected.setDiscipline(discipline);
		
		Set<Discipline> disciplines = new HashSet<Discipline>();
		disciplines.add(discipline);
		term.setTermName("secondTestTerm");
		term.setDisciplines(disciplines);
		this.termDAO.save(term);
		expected.setTerm(term);
		
		expected.setRating(50);
		
		// WHEN
		this.ratingDAO.save(expected);
		
		expected = this.ratingDAO.get(expected.getId());
		Integer newRating = 40;
		expected.setRating(newRating);
		this.ratingDAO.save(expected);
		
		this.sessinFactory.getCurrentSession().flush();
		this.sessinFactory.getCurrentSession().clear();
		
		// THEN
		expected = this.ratingDAO.get(expected.getId());
		
		Assert.assertEquals(newRating, expected.getRating());
	}
	
	@Test
	@Transactional
	public void getByTermAndStudId() throws Exception {
		// GIVEN
		Rating expected = new Rating();
		Discipline discipline = new Discipline();
		Term term = new Term();
		Student student = new Student();
		
		SimpleDateFormat FORMATTER = new SimpleDateFormat("dd/MM/yyyy");
		Date admissionDate = FORMATTER.parse("11/11/2011");
		
		student.setAdmissionDate(admissionDate);
		student.setLastName("thirdTest");
		student.setName("thirdTest2");
		student.setUniversityGroup("thirdTestGroup");
		this.studentDAO.save(student);
		expected.setStudent(student);
		
		discipline.setDisciplineName("thirdTestDiscipline");
		this.disciplineDAO.save(discipline);
		expected.setDiscipline(discipline);
		
		Set<Discipline> disciplines = new HashSet<Discipline>();
		disciplines.add(discipline);
		term.setTermName("thirdTestTerm");
		term.setDisciplines(disciplines);
		this.termDAO.save(term);
		expected.setTerm(term);
		
		expected.setRating(50);
		
		// WHEN
		this.ratingDAO.save(expected);
		List<Rating> actual = this.ratingDAO.getByTermAndStudId(student.getId(), term.getId());
		// THEN
		
		Assert.assertEquals(expected.getRating(), actual.get(0).getRating());
		Assert.assertEquals(expected.getStudent().getName(), actual.get(0).getStudent().getName());
		Assert.assertEquals(expected.getTerm().getTermName(), actual.get(0).getTerm().getTermName());
		Assert.assertEquals(expected.getDiscipline().getDisciplineName(), actual.get(0).getDiscipline().getDisciplineName());
	}
	
	@Test
	@Transactional
	public void delete() throws Exception {
		// GIVEN
		Rating expected = new Rating();
		Discipline discipline = new Discipline();
		Term term = new Term();
		Student student = new Student();
		
		SimpleDateFormat FORMATTER = new SimpleDateFormat("dd/MM/yyyy");
		Date admissionDate = FORMATTER.parse("11/11/2011");
		
		student.setAdmissionDate(admissionDate);
		student.setLastName("thirdTest");
		student.setName("thirdTest2");
		student.setUniversityGroup("thirdTestGroup");
		this.studentDAO.save(student);
		expected.setStudent(student);
		
		discipline.setDisciplineName("thirdTestDiscipline");
		this.disciplineDAO.save(discipline);
		expected.setDiscipline(discipline);
		
		Set<Discipline> disciplines = new HashSet<Discipline>();
		disciplines.add(discipline);
		term.setTermName("thirdTestTerm");
		term.setDisciplines(disciplines);
		this.termDAO.save(term);
		expected.setTerm(term);
		
		expected.setRating(50);
		
		// WHEN
		this.ratingDAO.save(expected);
		
		this.sessinFactory.getCurrentSession().flush();
		this.sessinFactory.getCurrentSession().clear();
		
		// THEN
		String ids = String.valueOf(expected.getId());
		
		this.ratingDAO.delete(ids);
		this.sessinFactory.getCurrentSession().flush();
		this.sessinFactory.getCurrentSession().clear();
		
		Rating actual = this.ratingDAO.get(expected.getId());
		
		Assert.assertEquals(null, actual);
	}
	
	@Test
	@Transactional
	public void get() throws Exception {
		// GIVEN
		Rating expected = new Rating();
		Discipline discipline = new Discipline();
		Term term = new Term();
		Student student = new Student();
		
		SimpleDateFormat FORMATTER = new SimpleDateFormat("dd/MM/yyyy");
		Date admissionDate = FORMATTER.parse("11/11/2011");
		
		student.setAdmissionDate(admissionDate);
		student.setLastName("thirdTest");
		student.setName("thirdTest2");
		student.setUniversityGroup("thirdTestGroup");
		this.studentDAO.save(student);
		expected.setStudent(student);
		
		discipline.setDisciplineName("thirdTestDiscipline");
		this.disciplineDAO.save(discipline);
		expected.setDiscipline(discipline);
		
		Set<Discipline> disciplines = new HashSet<Discipline>();
		disciplines.add(discipline);
		term.setTermName("thirdTestTerm");
		term.setDisciplines(disciplines);
		this.termDAO.save(term);
		expected.setTerm(term);
		
		expected.setRating(50);
		
		// WHEN
		this.ratingDAO.save(expected);
		
		this.sessinFactory.getCurrentSession().flush();
		this.sessinFactory.getCurrentSession().clear();
		
		// THEN
		expected = this.ratingDAO.get(expected.getId());
		
		
		// THEN
		Rating actual = this.ratingDAO.get(expected.getId());
		
		Assert.assertEquals(expected.getDiscipline(), actual.getDiscipline());
		Assert.assertEquals(expected.getRating(), actual.getRating());
		Assert.assertEquals(expected.getStudent(), actual.getStudent());
	}

}
