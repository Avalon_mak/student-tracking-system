package com.students.data.discipline.dao;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.students.data.discipline.model.Discipline;
import com.students.data.student.model.Student;

import org.junit.Assert;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:application-context.xml" })
@EnableTransactionManagement
public class DisciplineDAOImplTest {

	@Autowired
	private DisciplineDAO dao;

	@Autowired
	private SessionFactory sessionFactory;

	@Test
	@Transactional
	public void save() {
		// GIVEN
		Discipline expected = new Discipline();

		expected.setDisciplineName("firstTest");

		// WHEN
		this.dao.save(expected);

		// THEN
		Discipline actual = this.dao.get(expected.getId());
		Assert.assertEquals(expected.getDisciplineName(), actual.getDisciplineName());
	}

	@Test
	@Transactional
	public void update() {
		// GIVEN
		Discipline expected = new Discipline();

		expected.setDisciplineName("firstTest");

		// WHEN
		this.dao.save(expected);
		this.sessionFactory.getCurrentSession().flush();
		this.sessionFactory.getCurrentSession().clear();

		expected = this.dao.get(expected.getId());
		expected.setDisciplineName("firstTestEdited");
		this.dao.save(expected);
		this.sessionFactory.getCurrentSession().flush();
		this.sessionFactory.getCurrentSession().clear();

		// THEN
		Discipline actual = this.dao.get(expected.getId());

		Assert.assertEquals(expected.getDisciplineName(), actual.getDisciplineName());
	}

	@Test
	@Transactional
	public void delete() {
		// GIVEN
		Discipline toDb = new Discipline();

		toDb.setDisciplineName("thirdTest");
		this.dao.save(toDb);

		// WHEN
		toDb = this.dao.get(toDb.getId());

		List<Long> ids = new ArrayList<Long>();
		ids.add(toDb.getId());
		this.dao.delete(ids);
		this.sessionFactory.getCurrentSession().clear();

		// THEN
		Discipline actual = this.dao.get(toDb.getId());

		Assert.assertEquals(null, actual);
	}

	@Test
	@Transactional
	public void listAll() {
		// GIVEN
		Discipline toDb = new Discipline();
		Discipline toDb2 = new Discipline();
		Discipline toDb3 = new Discipline();
		Discipline toDb4 = new Discipline();

		toDb.setDisciplineName("fouthTest");
		toDb2.setDisciplineName("fouthTest2");
		toDb3.setDisciplineName("fouthTest3");
		toDb4.setDisciplineName("fouthTest4");

		// save data to DB
		this.dao.save(toDb);
		this.dao.save(toDb2);
		this.dao.save(toDb3);
		this.dao.save(toDb4);

		this.sessionFactory.getCurrentSession().flush();
		this.sessionFactory.getCurrentSession().clear();

		// WHEN
		List<Discipline> all = this.dao.listAll();
		int actualSize = all.size();

		// THEN
		Assert.assertEquals(4, actualSize);
	}

	@Test
	@Transactional
	public void get() {
		// GIVEN
		Discipline expected = new Discipline();
		expected.setDisciplineName("fifthTest");
		this.dao.save(expected);

		// WHEN
		Discipline actual = this.dao.get(expected.getId());

		// THEN
		Assert.assertEquals(expected.getDisciplineName(), actual.getDisciplineName());
	}

	@Test
	@Transactional
	public void getByName() {
		// GIVEN
		Discipline expected = new Discipline();
		expected.setDisciplineName("sixthTest");

		this.dao.save(expected);

		// WHEN
		Set<Discipline> actual = this.dao.getByName(expected.getDisciplineName());
		Iterator<Discipline> i = actual.iterator();
		List<Discipline> actualList = new ArrayList<>();
		while(i.hasNext()){
			actualList.add(i.next());
		}
		
		// THEN
		Assert.assertEquals(expected.getDisciplineName(), actualList.get(0).getDisciplineName());
	}
}
