package com.students.data.term.dao;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.students.data.discipline.dao.DisciplineDAO;
import com.students.data.discipline.model.Discipline;
import com.students.data.term.model.Term;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:application-context.xml" })
@EnableTransactionManagement
public class TermDAOImplTest {

	@Autowired
	private TermDAO dao;

	@Autowired
	private SessionFactory sessionFactory;

	@Test
	@Transactional
	public void save() {
		// GIVEN
		Set<Discipline> disciplines = new HashSet<Discipline>();
		Discipline one = new Discipline();
		Discipline two = new Discipline();
		Discipline three = new Discipline();

		disciplines.add(one);
		disciplines.add(two);
		disciplines.add(three);

		one.setDisciplineName("firstTest");
		two.setDisciplineName("firstTest2");
		three.setDisciplineName("firstTest3");

		Term expected = new Term();
		expected.setTermName("firstTestTerm");
		expected.setDisciplines(disciplines);

		this.dao.save(expected);
		// WHEN

		Term actual = this.dao.get(expected.getId());

		// THEN

		Assert.assertEquals(expected.getTermName(), actual.getTermName());
		Assert.assertEquals(expected.getDisciplines(), actual.getDisciplines());
	}

	@Test
	@Transactional
	public void update() {
		// GIVEN
		Set<Discipline> disciplines = new HashSet<Discipline>();
		Discipline one = new Discipline();
		Discipline two = new Discipline();
		Discipline three = new Discipline();

		disciplines.add(one);
		disciplines.add(two);
		disciplines.add(three);

		one.setDisciplineName("secondTest");
		two.setDisciplineName("secondTest2");
		three.setDisciplineName("secondTest3");

		Term expected = new Term();
		expected.setTermName("secondTestTerm");
		expected.setDisciplines(disciplines);

		this.dao.save(expected);
		// WHEN

		String newTermName = "secondTermNameEdited";
		Term actual = this.dao.get(expected.getId());
		actual.setTermName(newTermName);

		// THEN
		Assert.assertEquals(expected.getTermName(), newTermName);
	}

	@Test
	@Transactional
	public void deleteDisciplineFromTerm() {
		// GIVEN
		Set<Discipline> disciplines = new HashSet<Discipline>();
		Discipline one = new Discipline();
		Discipline two = new Discipline();
		Discipline three = new Discipline();

		disciplines.add(one);
		disciplines.add(two);
		disciplines.add(three);

		one.setDisciplineName("thirdTest");
		two.setDisciplineName("thirdTest2");
		three.setDisciplineName("thirdTest3");

		Term expected = new Term();
		expected.setTermName("thirdTestTerm");
		expected.setDisciplines(disciplines);

		this.dao.save(expected);
		// WHEN

		expected = this.dao.get(expected.getId());

		Set<Discipline> newDisciplines = new HashSet<Discipline>();
		newDisciplines.add(one);
		newDisciplines.add(two);
		expected.setDisciplines(newDisciplines);

		this.dao.save(expected);
		int expectedSize = 2;
		// THEN

		Term actual = this.dao.get(expected.getId());

		Assert.assertEquals(expectedSize, actual.getDisciplines().size());
	}

	@Test
	@Transactional
	public void delete() {
		// GIVEN
		Set<Discipline> disciplines = new HashSet<Discipline>();
		Discipline one = new Discipline();
		Discipline two = new Discipline();
		Discipline three = new Discipline();

		disciplines.add(one);
		disciplines.add(two);
		disciplines.add(three);

		one.setDisciplineName("fourthTest");
		two.setDisciplineName("fourthTest2");
		three.setDisciplineName("fourthTest3");

		Term toDb = new Term();
		toDb.setTermName("fourthTestTerm");
		toDb.setDisciplines(disciplines);

		this.dao.save(toDb);

		// WHEN
		toDb = this.dao.get(toDb.getId());

		List<Long> ids = new ArrayList<Long>();
		ids.add(toDb.getId());
		this.dao.deleteTermsByIds(ids);
		this.sessionFactory.getCurrentSession().clear();

		// THEN
		Term actual = this.dao.get(toDb.getId());

		Assert.assertEquals(null, actual);
	}
	
	@Test
	@Transactional
	public void listAll(){
		// GIVEN
		Set<Discipline> disciplines = new HashSet<Discipline>();
		Set<Discipline> disciplines2 = new HashSet<Discipline>();
		Set<Discipline> disciplines3 = new HashSet<Discipline>();
		Discipline one = new Discipline();
		Discipline two = new Discipline();
		Discipline three = new Discipline();

		disciplines.add(one);
		disciplines.add(two);
		disciplines.add(three);

		disciplines2.add(two);
		disciplines2.add(three);

		disciplines3.add(one);
		disciplines3.add(three);

		one.setDisciplineName("fifthTest");
		two.setDisciplineName("fifthTest2");
		three.setDisciplineName("fifthTest3");

		Term toDb = new Term();
		Term toDb2 = new Term();
		Term toDb3 = new Term();
		
		
		toDb.setTermName("fifthTestTerm");
		toDb.setDisciplines(disciplines);

		toDb2.setTermName("fifthTestTerm2");
		toDb2.setDisciplines(disciplines2);
		
		toDb3.setTermName("fifthTestTerm2");
		toDb3.setDisciplines(disciplines2);
		
		this.dao.save(toDb);
		this.dao.save(toDb2);
		this.dao.save(toDb3);
		
		// WHEN
		int expectedSize = 3;
		List<Term> all = this.dao.listAll();
		
		// THEN
		Assert.assertEquals(expectedSize, all.size());
	}
	
	@Test
	@Transactional
	public void get() {
		// GIVEN
		Set<Discipline> disciplines = new HashSet<Discipline>();
		Discipline one = new Discipline();
		Discipline two = new Discipline();
		Discipline three = new Discipline();

		disciplines.add(one);
		disciplines.add(two);
		disciplines.add(three);

		one.setDisciplineName("fourthTest");
		two.setDisciplineName("fourthTest2");
		three.setDisciplineName("fourthTest3");

		Term expected = new Term();
		expected.setTermName("fourthTestTerm");
		expected.setDisciplines(disciplines);
		
		// WHEN
		this.dao.save(expected);
		Term actual = this.dao.get(expected.getId());
		
		// THEN
		Assert.assertEquals(expected.getTermName(), actual.getTermName());
		Assert.assertEquals(expected.getDisciplines(), actual.getDisciplines());
	}
	
	@Test
	@Transactional
	public void getByName() {
		// GIVEN
		Set<Discipline> disciplines = new HashSet<Discipline>();
		Discipline one = new Discipline();
		Discipline two = new Discipline();
		Discipline three = new Discipline();

		disciplines.add(one);
		disciplines.add(two);
		disciplines.add(three);

		one.setDisciplineName("fourthTest");
		two.setDisciplineName("fourthTest2");
		three.setDisciplineName("fourthTest3");

		Term expected = new Term();
		expected.setTermName("fourthTestTerm");
		expected.setDisciplines(disciplines);
		this.dao.save(expected);
		
		// WHEN
		List <Term> actual = this.dao.getByName(expected.getTermName());
	
		// THEN
		Assert.assertEquals(expected.getTermName(), actual.get(0).getTermName());
		Assert.assertEquals(expected.getDisciplines(), actual.get(0).getDisciplines());
	}

}