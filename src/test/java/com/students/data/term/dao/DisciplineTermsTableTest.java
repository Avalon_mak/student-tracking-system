package com.students.data.term.dao;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.students.data.discipline.dao.DisciplineDAO;
import com.students.data.discipline.model.Discipline;
import com.students.data.term.model.Term;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:application-context.xml" })
@EnableTransactionManagement
public class DisciplineTermsTableTest {

	@Autowired
	private DisciplineDAO disciplineDAO;

	@Autowired
	private TermDAO termDAO;

	@Autowired
	private SessionFactory sessionFactory;

	@Test
	@Transactional
	public void save() {
		// GIVEN
		Discipline one = new Discipline();
		Discipline two = new Discipline();
		Discipline three = new Discipline();

		Set<Discipline> disciplines = new HashSet<Discipline>();

		one.setDisciplineName("firstTest");
		two.setDisciplineName("firstTest2");
		three.setDisciplineName("firstTest3");

		disciplines.add(one);
		disciplines.add(two);
		disciplines.add(three);

		Term expected = new Term();
		expected.setDisciplines(disciplines);
		expected.setTermName("testTermOne");

		this.disciplineDAO.save(one);
		this.disciplineDAO.save(two);
		this.disciplineDAO.save(three);

		this.termDAO.save(expected);

		this.sessionFactory.getCurrentSession().flush();
		this.sessionFactory.getCurrentSession().clear();

		// WHEN
		List<Term> allTerms = new ArrayList<Term>();
		Iterator<Term> i;
		Set<Term> terms;
		Discipline oneFromDb = this.disciplineDAO.get(one.getId());
		terms = oneFromDb.getTerms();
		i = terms.iterator();
		while (i.hasNext()) {
			allTerms.add(i.next());
		}

		Discipline twoFromDb = this.disciplineDAO.get(two.getId());
		terms = twoFromDb.getTerms();
		i = terms.iterator();
		while (i.hasNext()) {
			allTerms.add(i.next());
		}

		Discipline threeFromDb = this.disciplineDAO.get(three.getId());
		terms = threeFromDb.getTerms();
		i = terms.iterator();
		while (i.hasNext()) {
			allTerms.add(i.next());
		}

		expected = this.termDAO.get(expected.getId());

		// THEN

		Assert.assertEquals(expected.getId(), allTerms.get(0).getId());
		Assert.assertEquals(expected.getId(), allTerms.get(1).getId());
		Assert.assertEquals(expected.getId(), allTerms.get(2).getId());
	}

	@Test
	@Transactional
	public void deleteDiscipline() {
		// GIVEN
		Discipline one = new Discipline();
		Discipline two = new Discipline();
		Discipline three = new Discipline();

		Set<Discipline> disciplines = new HashSet<Discipline>();

		one.setDisciplineName("secondTest");
		two.setDisciplineName("secondTest2");
		three.setDisciplineName("secondTest3");

		disciplines.add(one);
		disciplines.add(two);
		disciplines.add(three);

		Term expected = new Term();
		expected.setDisciplines(disciplines);
		expected.setTermName("secondTermOne");

		this.disciplineDAO.save(one);
		this.disciplineDAO.save(two);
		this.disciplineDAO.save(three);

		this.termDAO.save(expected);

		this.sessionFactory.getCurrentSession().flush();
		this.sessionFactory.getCurrentSession().clear();

		// WHEN
		List<Long> ids = new ArrayList<Long>();
		ids.add(one.getId());
		this.disciplineDAO.delete(ids);

		this.sessionFactory.getCurrentSession().flush();
		this.sessionFactory.getCurrentSession().clear();

		expected = this.termDAO.get(expected.getId());
		// THEN

		Assert.assertEquals(2, expected.getDisciplines().size());
	}

	@Test
	@Transactional
	public void changeDisciplines() {
		// GIVEN
		Discipline one = new Discipline();
		Discipline two = new Discipline();
		Discipline three = new Discipline();

		Set<Discipline> disciplines = new HashSet<Discipline>();

		one.setDisciplineName("thirdTest");
		two.setDisciplineName("thirdTest2");
		three.setDisciplineName("thirdTest3");

		disciplines.add(one);
		disciplines.add(two);
		disciplines.add(three);

		Term expected = new Term();
		expected.setDisciplines(disciplines);
		expected.setTermName("thirdTermOne");

		this.disciplineDAO.save(one);
		this.disciplineDAO.save(two);
		this.disciplineDAO.save(three);

		this.termDAO.save(expected);

		this.sessionFactory.getCurrentSession().flush();
		this.sessionFactory.getCurrentSession().clear();

		// WHEN
		expected = this.termDAO.get(expected.getId());
		disciplines.remove(one);
		Discipline four = new Discipline();
		four.setDisciplineName("thirdTest4");
		this.disciplineDAO.save(four);
		disciplines.add(four);
		expected.setDisciplines(disciplines);
		this.termDAO.save(expected);

		this.sessionFactory.getCurrentSession().flush();
		this.sessionFactory.getCurrentSession().clear();
		
		expected = this.termDAO.get(expected.getId());
		// THEN
		List<Discipline> expectedDisciplines = new ArrayList<Discipline>();
		List<Discipline> actualDisciplines = new ArrayList<Discipline>();
		actualDisciplines.add(two);
		actualDisciplines.add(three);
		actualDisciplines.add(four);
		Iterator<Discipline> i = expected.getDisciplines().iterator();
		while(i.hasNext()){
			expectedDisciplines.add(i.next());
		}
		
		Assert.assertEquals(actualDisciplines.size(), expectedDisciplines.size());
		boolean match = false;
		for(Discipline d: expectedDisciplines){
			if(d.getDisciplineName().equals(four.getDisciplineName())){
				Assert.assertEquals(true, true);
				match = true;
			}
		}
		
		if (!match) {
			Assert.assertEquals(false, true);
		}
	}
}
